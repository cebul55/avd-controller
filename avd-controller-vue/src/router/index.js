import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import Settings from "@/views/Settings";
import Status from "@/views/Status";
import Sequences from "@/views/Sequences";
import FileAnalysis from "@/views/FileAnalysis";
import SequenceRunner from "@/views/SequenceRunner";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/settings",
    name: "Settings",
    component: Settings,
  },
  {
    path: "/status",
    name: "Status",
    component: Status,
  },
  {
    path: "/sequences",
    name: "Sequences",
    component: Sequences,
  },
  {
    path: "/analysis",
    name: "FileAnalysis",
    component: FileAnalysis,
  },
  {
    path: "/sequenceRunner",
    name: "SequenceRunner",
    component: SequenceRunner,
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
