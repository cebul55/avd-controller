import Vue from "vue";
import App from "./App.vue";
import "./registerServiceWorker";
import router from "./router";
import store from "./store";
import vuetify from "./plugins/vuetify";
import axios from "axios";
import "roboto-fontface/css/roboto/roboto-fontface.css";
import "@mdi/font/css/materialdesignicons.css";
import AdbService from "@/services/AdbService";

Vue.config.productionTip = false;

axios.defaults.baseURL = process.env.VUE_APP_API_URL;
axios.defaults.crossDomain = true;

new Vue({
  router,
  store,
  vuetify,
  render: (h) => h(App),
  methods: {
    async getDefaultDeviceName() {
      AdbService.getDefaultDeviceName()
        .then((result) => {
          this.avdName = result;
          console.log(result);
        })
        .catch((error) => {
          console.log(error);
        });
    },
  },
  mounted() {
    this.getDefaultDeviceName();
  },
}).$mount("#app");