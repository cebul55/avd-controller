import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    avdName: "Pixel_3a_API_30",
    port: 5554,
    deviceId: "emulator-5554",
  },
  mutations: {
    updateAvdName(state, newValue) {
      console.log("change old val:" + this.state.avdName + " to: " + newValue);
      this.state.avdName = newValue;
    },
    updatePort(state, newValue) {
      this.state.port = newValue;
    },
    updateDeviceId(state, newValue) {
      this.state.deviceId = newValue;
    },
  },
  actions: {},
  modules: {},
});
