import Vue from "vue";
import Vuetify from "vuetify/lib/framework";

import colors from "vuetify/es5/util/colors";

Vue.use(Vuetify);

export default new Vuetify({
  theme: {
    options: {
      customProperties: true,
    },
    themes: {
      // light: {
      //   primary: "#44b3ee",
      //   secondary: "#424242",
      //   accent: "#82B1FF",
      //   error: "#FF5252",
      //   info: "#2196F3",
      //   success: "#4CAF50",
      //   warning: "#FFC107",
      // },
      light: {
        primary: "#03a9f4",
        secondary: colors.blueGrey.lighten1,
        accent: "#3f51b5",
        error: "#f44336",
        warning: "#ffc107",
        info: "#607d8b",
        success: "#8bc34a",
      },
      dark: {
        primary: colors.blueGrey,
      },
    },
  },
});
