import axios from "axios";

export default {
  async getDeviceMonitorData(deviceName) {
    return axios
      .get(`/api/devicestate/${deviceName}`)
      .then((result) => {
        return result.data;
      })
      .catch((error) => {
        throw error;
      });
  },
  async startDeviceMonitor(deviceName) {
    return axios
      .get(`/api/devicestate/${deviceName}/monitor`)
      .then((result) => {
        console.log(result);
      })
      .catch((error) => {
        throw error;
      });
  },
  async stopDeviceMonitor(deviceName) {
    return axios
      .post(`/api/devicestate/${deviceName}`)
      .then((result) => {
        console.log(result);
      })
      .catch((error) => {
        throw error;
      });
  },
  async getDeviceDetectorData(deviceName) {
    return axios
      .get(`/api/devicestate/${deviceName}/detector`)
      .then((result) => {
        return result.data;
      })
      .catch((error) => {
        throw error;
      });
  },
};
