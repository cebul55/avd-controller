import axios from "axios";

export default {
  async getAnalysisForSha(sha) {
    return axios(`/api/koodous/analysis/${sha}`)
      .then((result) => {
        return result.data;
      })
      .catch((error) => {
        console.log(error);
      });
  },
  async getFeedForSha(sha) {
    return axios(`/api/koodous/votes/${sha}`)
      .then((result) => {
        return result.data;
      })
      .catch((error) => {
        console.log(error);
      });
  },
};
