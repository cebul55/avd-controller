import axios from "axios";

export default {
  async getSensors() {
    return axios("/api/sensor/getSensors")
      .then((result) => {
        console.log(result.data);
        return result.data;
      })
      .catch((error) => {
        console.log(error);
      });
  },
};
