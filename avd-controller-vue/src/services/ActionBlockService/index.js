import axios from "axios";

export default {
  async addActionBlock(actionBlock) {
    let data = {
      commandAction: actionBlock.commandAction,
      sleepSeconds: actionBlock.sleepSeconds,
      subCommand1: actionBlock.subCommand1,
      subCommand2: actionBlock.subCommand2,
      subCommand3: actionBlock.subCommand3,
      type: actionBlock.type,
    };
    let config = {
      headers: {
        Accept: "application/json;charset=utf-8",
      },
    };
    return axios
      .post(`/api/actionBlock/add`, data, config)
      .then((result) => {
        return result.data;
      })
      .catch((error) => {
        throw error;
      });
  },
  async updateActionBlock(actionBlock) {
    let data = {
      id: actionBlock.id,
      commandAction: actionBlock.commandAction,
      sleepSeconds: actionBlock.sleepSeconds,
      subCommand1: actionBlock.subCommand1,
      subCommand2: actionBlock.subCommand2,
      subCommand3: actionBlock.subCommand3,
      type: actionBlock.type,
      nextAction: actionBlock.nextAction,
      nextActionId: actionBlock.nextActionId,
    };
    let config = {
      headers: {
        Accept: "application/json;charset=utf-8",
      },
    };
    return axios
      .post(`/api/actionBlock/update/${actionBlock.id}`, data, config)
      .then((result) => {
        return result.data;
      })
      .catch((error) => {
        throw error;
      });
  },
  async addNextAction(parentId, childAction) {
    let data = {
      id: childAction.id,
      commandAction: childAction.commandAction,
      sleepSeconds: childAction.sleepSeconds,
      subCommand1: childAction.subCommand1,
      subCommand2: childAction.subCommand2,
      subCommand3: childAction.subCommand3,
      type: childAction.type,
      nextAction: childAction.nextAction,
      nextActionId: childAction.nextActionId,
    };
    let config = {
      headers: {
        Accept: "application/json;charset=utf-8",
      },
    };
    return axios
      .post(`/api/actionBlock/addNextAction/${parentId}`, data, config)
      .then((result) => {
        return result.data;
      })
      .catch((error) => {
        throw error;
      });
  },
  async deleteActionBlock(actionBlock) {
    // todo change request type
    let config = {
      headers: {
        Accept: "application/json;charset=utf-8",
      },
    };
    debugger;
    return axios.post(`/api/actionBlock/delete/${actionBlock.id}`, {}, config)
      .then((result) => {
        console.log(
          "successfully deleted action block " + actionBlock.id + ";" + result
        );
      })
      .catch((error) => {
        throw error;
      });
  },
};
