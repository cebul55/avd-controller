import axios from "axios";

export default {
  async getAnalysisForSha(sha) {
    let config = {
      headers: {
        Accept: "application/json;charset=utf-8",
        "Keep-Alive": "timeout=20",
        Connection: "keep-alive",
      },
    };
    // return axios(`/api/virustotal/file/${sha}`)
    return axios
      .get(`/api/virustotal/file/${sha}`, config)
      .then((result) => {
        debugger;
        return result.data;
      })
      .catch((error) => {
        throw error;
      });
  },
};
