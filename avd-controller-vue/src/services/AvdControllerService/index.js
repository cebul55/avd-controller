import axios from "axios";

export default {
  async connectAvdController() {
    return axios
      .get("/api/avd/connect")
      .then((result) => {
        console.log(result);
        return result.data.status === "OK";
      })
      .catch((error) => {
        throw error;
      });
  },
  async disconnectAvdController() {
    return axios
      .get("/api/avd/disconnect")
      .then((result) => {
        console.log(result);
        return result.data.status === "OK";
      })
      .catch((error) => {
        throw error;
      });
  },
  async isConnected() {
    return axios
      .get("/api/avd/status")
      .then((result) => {
        return result.data.response === "Connected";
      })
      .catch((error) => {
        throw error;
      });
  },
  async sendExampleCommand() {
    return axios
      .get("/api/avd/sendCommand", {
        params: {
          command: "sms send 123 test",
        },
      })
      .then((result) => {
        console.log(result);
        return result.data.status === "OK";
      })
      .catch((error) => {
        throw error;
      });
  },
};
