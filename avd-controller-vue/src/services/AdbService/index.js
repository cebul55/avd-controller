import axios from "axios";
const FormData = require("form-data");

export default {
  async getDefaultDeviceName() {
    return axios("/api/emulator/default")
      .then((result) => {
        return result.data;
      })
      .catch((error) => {
        throw error;
      });
  },
  async getAvailableDevices() {
    return axios("/api/emulator/")
      .then((result) => {
        return result.data;
      })
      .catch((error) => {
        throw error;
      });
  },
  async getInstalledPackages(deviceId) {
    return axios
      .get(`/api/adb/${deviceId}/installedPackages`)
      .then((result) => {
        return result.data;
      })
      .catch((error) => {
        throw error;
      });
  },
  async runPackage(deviceId, activityName) {
    return axios
      .post(`/api/adb/${deviceId}/runPackage`, null, {
        params: {
          activityName: activityName,
        },
      })
      .then((result) => {
        return result.data;
      })
      .catch((error) => {
        throw error;
      });
  },
  async stopPackage(deviceId, activityName) {
    return axios
      .post(`/api/adb/${deviceId}/stopActivity`, null, {
        params: {
          activityName: activityName,
        },
      })
      .then((result) => {
        return result.data;
      })
      .catch((error) => {
        throw error;
      });
  },
  async installPackage(deviceId, apkFile) {
    const formData = new FormData();
    formData.append("apkFile", apkFile);

    return axios
      .post(`/api/adb/${deviceId}/installPackage`, formData, {
        headers: { "Content-Type": "multipart/form-data" },
      })
      .then((result) => {
        return result.data;
      })
      .catch((error) => {
        throw error;
      });
  },
  async uninstallPackage(deviceId, packageName) {
    return axios
      .get(`/api/adb/${deviceId}/uninstallPackage`, {
        params: {
          packageName: packageName,
        },
      })
      .then((result) => {
        return result.data;
      })
      .catch((error) => {
        throw error;
      });
  },
};
