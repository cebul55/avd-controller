import axios from "axios";

export default {
  async getLogFile(type) {
    return axios
      .get(`/api/filestorage/logs/${type}`)
      .then((result) => {
        return result.data;
      })
      .catch((error) => {
        throw error;
      });
  },
};
