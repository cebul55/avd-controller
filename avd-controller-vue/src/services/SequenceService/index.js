import axios from "axios";

export default {
  async getSequences() {
    return axios("/api/sequence/")
      .then((result) => {
        console.log(result.data);
        return result.data;
      })
      .catch((error) => {
        console.log(error);
      });
  },
  async getSequence(id){
    return axios(`api/sequence/${id}`)
      .then((result) => {
        console.log(result.data);
        return result.data;
      })
      .catch((error) => {
        console.log(error);
      });
  },
  async addSequence(sequence) {
    return axios.post(`/api/sequence/add`, sequence)
      .then((result) => {
        return result.data;
      })
      .catch((error) => {
        throw error;
      });
  },
  async deleteActions(sequenceId) {
    return axios(`/api/sequence/${sequenceId}/deleteAllActions`)
      .then((result) => {
        return result.data;
      })
      .catch((error) => {
        throw error;
      });
  },
  async deleteSequence(sequenceId) {
    return axios(`/api/sequence/${sequenceId}/deleteSequence`)
      .then((result) => {
        return result.data;
      })
      .catch((error) => {
        throw error;
      });
  },
  async addHead(sequenceId, actionBlock) {
    return axios.post(`/api/sequence/${sequenceId}/addHead`, actionBlock)
      .then((result) => {
        return result.data;
      })
      .catch((error) => {
        throw error;
      });
  },
  async updateActionList(sequenceId, actionIdList) {
    return axios.post(`/api/sequence/${sequenceId}/updateActionList`, actionIdList)
      .then((result) => {
        return result.data;
      })
      .catch((error) => {
        throw error;
      });
  },
  async runSequence(sequenceId){
    return axios(`/api/sequence/${sequenceId}/run`)
      .then((result) => {
        return result.data;
      })
      .catch((error) => {
        throw error;
      });
  },
  async stopSequence(sequenceId){
    return axios(`/api/sequence/${sequenceId}/stop`)
      .then((result) => {
        return result.data;
      })
      .catch((error) => {
        throw error;
      });
  },
};
