import axios from "axios";

export default {
  async getAvailableDevices() {
    return axios
      .get("/api/emulator/")
      .then((result) => {
        return result.data;
      })
      .catch((error) => {
        throw error;
      });
  },
  async startAvd() {
    return axios
      .get("/api/emulator/startAvd")
      .then((result) => {
        console.log(result.data);
        return result.data.status === "OK";
      })
      .catch((error) => {
        console.log(error);
      });
  },
  async stopAvd() {
    return axios
      .get("/api/emulator/stopAvd")
      .then((result) => {
        console.log(result.data);
        return result.data.status === "OK";
      })
      .catch((error) => {
        console.log(error);
      });
  },
  async isRunning() {
    // todo name
    return axios
      .get("/api/emulator/status")
      .then((result) => {
        if(result.data.message && result.data.message === "Running")
          return true;
        else return false;
      })
      .catch((error) => {
        throw error;
      });
  },
};
