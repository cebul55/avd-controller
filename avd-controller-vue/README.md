# avd-controller-vue

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your unit tests
```
npm run test:unit
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
See [Create vuetify/elcectron app](https://itnext.io/electron-application-with-vue-js-and-vuetify-f2a1f9c749b8)