module.exports = {
  transpileDependencies: ["vuetify"],
  devServer: {
    port: "",
  },
  configureWebpack: {
    devtool: "source-map",
  },
};
