# Polecenia Emulator AVD
* Uruchomienie
```
emulator -avd Pixel_2_API_30 -debug init,metrics -port 5555
```

[Emulator command line](https://developer.android.com/studio/run/emulator-commandline)

* połączenie telnet
```
telnet 127.0.0.1 5555
```
następnie należy wprowadzić token znajdujący się w ścieżce np.:
```
'/Users/bartoszcybulski/.emulator_console_auth_token'
```