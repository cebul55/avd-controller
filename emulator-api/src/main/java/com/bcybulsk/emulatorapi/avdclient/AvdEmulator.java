package com.bcybulsk.emulatorapi.avdclient;

import com.bcybulsk.emulatorapi.model.devicestate.DeviceState;
import com.bcybulsk.emulatorapi.model.devicestate.DeviceStateMap;
import lombok.NoArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

@Component
@NoArgsConstructor
public class AvdEmulator {

    private static final Logger _log = LogManager.getLogger(AvdEmulator.class);

    @Value("${avd.emulator_telnet_port}")
    private String AVD_DEFAULT_PORT;

    @Value("${avd.default_cli}")
    private String DEFAULT_CLI;

    @Value("${avd.error_log}")
    private String errorFilePath;

    @Value("${avd.output_log}")
    private String outputFilePath;

    @Value("${avd.logcatPath")
    private String logcatFilePath;

    @Value("${avd.device_name}")
    private String deviceName;

    private ProcessBuilder processBuilder;
    private ProcessBuilder logcatProcessBuilder;
    private Process avdProcess;
    private Process logcatProcess;

    @Autowired
    private DeviceStateMap deviceStateMap;

    @PostConstruct
    public void init() throws Exception {
        this.processBuilder = new ProcessBuilder(DEFAULT_CLI, "-c", "emulator -avd " + deviceName + " -debug init," +
                "metrics" +
                " -port " +
                AVD_DEFAULT_PORT);
        this.logcatProcessBuilder = new ProcessBuilder(DEFAULT_CLI, "-c", "adb logcat \"*:W\"");
        redirectOutputAndError();
    }

    private void redirectOutputAndError() throws Exception {
        this.processBuilder.redirectError(new File(errorFilePath));
        this.processBuilder.redirectOutput(new File(outputFilePath));
        _log.info("AvdEmulatorController redirected error to file :" + errorFilePath);
        _log.info("AvdEmulatorController redirected output to file :" + outputFilePath);
        this.logcatProcessBuilder.redirectOutput(new File(logcatFilePath));
    }

    public boolean isProcessRunning(){
        return this.avdProcess != null && this.avdProcess.isAlive();
    }

    public void startAvd() throws IOException {
        if(isProcessRunning())
            return;
        this.avdProcess = processBuilder.start();
        this.logcatProcess = logcatProcessBuilder.start();
        putNewDeviceState(deviceName);
        _log.info("AvdEmulatorController started avd emulator");
    }

    private void putNewDeviceState(String deviceName) {
        DeviceState deviceState = new DeviceState();
        deviceState.setDeviceId(deviceName);
        deviceStateMap.putDeviceState(deviceName, deviceState);
    }

    public Process getAvdProcess() {
        return this.avdProcess;
    }

    public void stopAvd() {
        if(isProcessRunning()) {
            this.avdProcess.destroy();
        }
        deviceStateMap.popDeviceState(deviceName);
        _log.info("AvdEmulatorController stopped avd emulator");
        this.avdProcess = null;
    }

    public List<String> getAvailableAvds() throws IOException {
        ProcessBuilder processBuilder = new ProcessBuilder(DEFAULT_CLI, "-c", "emulator -list-avds");
        Process p = processBuilder.start();

        BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
        StringBuilder stringBuilder = new StringBuilder();
        String line = null;
        List<String> result = new ArrayList<String>();
        while((line = reader.readLine()) != null) {
            result.add(line);
        }
        return result;
    }

}
