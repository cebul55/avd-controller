package com.bcybulsk.emulatorapi;

import com.bcybulsk.emulatorapi.presentation.storage.StorageProperties;
import com.bcybulsk.emulatorapi.presentation.storage.StorageService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@SpringBootApplication
public class EmulatorApiApplication extends SpringBootServletInitializer {

    @Value("${avd.frontend_origin}")
    String frontendOrigin;

    public static void main(String[] args) {
        SpringApplication.run(EmulatorApiApplication.class, args);

//        AvdTelnetClientController controller = new AvdTelnetClientController();
//        controller.setUpConnection();
        // TODO setu up logs - or configure log4j2 on spring app.
    }

    @Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurer() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/api/**")
                        .allowedOrigins(frontendOrigin)
                        .allowedMethods("GET", "POST", "PUT",
                        "DELETE", "OPTIONS");
            }
        };
    }

    @Bean
    CommandLineRunner init(StorageService storageService) {
        return(args -> {
            storageService.deleteAll();
            storageService.init();
        });
    }


    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder builder) {
        return builder.build();
    }

//    @Bean
//    public CommandLineRunner run(RestTemplate restTemplate) throws Exception {
//        return args -> {
//            Quote quote = restTemplate.getForObject(
//                    "https://quoters.apps.pcfone.io/api/random", Quote.class);
//            log.info(quote.toString());
//        };
//    }

}
