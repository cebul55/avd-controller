package com.bcybulsk.emulatorapi.controller;

import com.bcybulsk.emulatorapi.model.sensor.Sensor;
import com.bcybulsk.emulatorapi.model.sensor.SensorService;
import com.bcybulsk.emulatorapi.presentation.sensor.SensorDto;
import com.bcybulsk.emulatorapi.presentation.sensor.SensorMapper;
import com.bcybulsk.emulatorapi.system.Endpoint;
import com.bcybulsk.emulatorapi.telnetclient.ResponseStatus;
import com.bcybulsk.emulatorapi.telnetclient.AvdTelnetClientService;
import com.bcybulsk.emulatorapi.util.ResponseBodyBuilder;
import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.IOException;
import java.util.Map;

@RestController
@RequiredArgsConstructor
@RequestMapping(AndroidVirtualDeviceController.ANDORIDVIRTUALDEVICE_URI)
public class AndroidVirtualDeviceController {

    private static final Logger _log = LogManager.getLogger(AndroidVirtualDeviceController.class);
    static final String ANDORIDVIRTUALDEVICE_URI = Endpoint.API_ROOT + "/avd";

    private final AvdTelnetClientService avdTelnetClientService;
    private final SensorService sensorService;
    private final SensorMapper sensorMapper;

    @PostConstruct
    public void init() throws IOException {
        sensorService.deleteAll();
//        controller = new AvdTelnetClientController();
//        avdTelnetClientController.setUpConnection();
//        _log.debug("Connection set up on initialization.");
    }

    private void setUpSensorStatus() throws IOException {
        sensorService.deleteAll();
        for(Sensor.SensorName sensorName: Sensor.SensorName.values()) {
            SensorDto sensor = avdTelnetClientService.checkSensor(sensorName);
            sensorService.add(sensorMapper.toSensor(sensor));
        }
    }

    @PreDestroy
    public void tearDown() {
        try {
            if(avdTelnetClientService.isConnected())
                avdTelnetClientService.tearDownConnection();
        } catch (Exception e) {
            _log.error("Failed to terminate connection with AVD. Connection may be terminated already.");
            _log.error(e.getMessage(), e);
        }
    }

    @GetMapping("/sendCommand")
    public ResponseEntity<Map<String, String>> sendCommand(String command) {
        try {
            if(!avdTelnetClientService.isConnected())
                return ResponseBodyBuilder.notConnected();

            avdTelnetClientService.sendCommand(command);
            Map<String, String> body = ResponseBodyBuilder.createResponseBody( ResponseStatus.OK.name(), "Command sending" +
                        " success.");
            return ResponseEntity.status(HttpStatus.OK).body(body);
        } catch (Exception e) {
            _log.error(e.getMessage(), e);
            Map<String, String> body = ResponseBodyBuilder.createResponseBody( ResponseStatus.ERROR.name(), "Unexpected error: " + e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(body);
        }
    }

    @GetMapping("/connect")
    public ResponseEntity<Map<String, String>> connect() {
        try {
            if(!avdTelnetClientService.isConnected()){
                avdTelnetClientService.setUpConnection();
                setUpSensorStatus();
            }

            Map<String, String> body = ResponseBodyBuilder.createResponseBody( ResponseStatus.OK.name(), "Successfully " +
                    "connected.");
            return ResponseEntity.status(HttpStatus.OK).body(body);
        } catch (Exception e) {
            _log.error(e.getMessage(), e);
            Map<String, String> body = ResponseBodyBuilder.createResponseBody( ResponseStatus.ERROR.name(), "Unexpected error: " + e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(body);
        }
    }

    @GetMapping("/disconnect")
    public ResponseEntity<Map<String, String>> disconnect() {
        try {
            if(avdTelnetClientService.isConnected())
                avdTelnetClientService.tearDownConnection();
            Map<String, String> body = ResponseBodyBuilder.createResponseBody( ResponseStatus.OK.name(), "Successfully " +
                    "disconnected.");
            return ResponseEntity.status(HttpStatus.OK).body(body);
        } catch (IOException e) {
            _log.error(e.getMessage(), e);
            Map<String, String> body = ResponseBodyBuilder.createResponseBody( ResponseStatus.ERROR.name(),
                    "Unable to disconnect. Unexpected error: " + e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(body);
        }
    }

    @GetMapping("status")
    public ResponseEntity<Map<String, String>> isConnected() {
        Map<String, String> body = null;
        if(avdTelnetClientService.isConnected())
            body = ResponseBodyBuilder.createResponseBody(ResponseStatus.OK.name(), "Connected");
        else
            body = ResponseBodyBuilder.createResponseBody(ResponseStatus.OK.name(), "Disconnected");
        return ResponseEntity.status(HttpStatus.OK).body(body);
    }
}
