package com.bcybulsk.emulatorapi.controller;

import com.bcybulsk.emulatorapi.avdclient.AvdEmulator;
import com.bcybulsk.emulatorapi.model.error.TechnicalException;
import com.bcybulsk.emulatorapi.system.Endpoint;
import com.bcybulsk.emulatorapi.telnetclient.ResponseStatus;
import com.bcybulsk.emulatorapi.util.ResponseBodyBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PreDestroy;
import java.io.IOException;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(AvdEmulatorController.AVDEMULATOR_URI)
public class AvdEmulatorController {
    private static final Logger _log = LogManager.getLogger(AvdEmulatorController.class);
    static final String AVDEMULATOR_URI = Endpoint.API_ROOT + "/emulator";

    private final AvdEmulator avdEmulator;
    @Value("${avd.device_name}")
    private String deviceName;

    public AvdEmulatorController(AvdEmulator avdEmulator) {
        this.avdEmulator = avdEmulator;
    }

    @PreDestroy
    public void tearDown() {
        this.avdEmulator.stopAvd();
    }

    @GetMapping("/default")
    public ResponseEntity<String> getDefaultAvd() {
        return ResponseEntity.ok(this.deviceName);
    }

    @GetMapping("/")
    public ResponseEntity<List<String>> getAvailableAvds() {
        try {
            return ResponseEntity.ok(
                    this.avdEmulator.getAvailableAvds()
            );
        } catch (IOException e) {
            _log.error(e.toString());
            throw new TechnicalException("error.unexpected");
        }
    }

    @GetMapping("/startAvd")
    public ResponseEntity<Map<String, String>> startAvd() {
        // todo starting defferent avd
        try {
            this.avdEmulator.startAvd();
            Map<String, String> body = ResponseBodyBuilder.createResponseBody( ResponseStatus.OK.name(),  "Successfully " +
                    "started AVD Emulator.");
            return ResponseEntity.status(HttpStatus.OK).body(body);
        } catch (Exception e) {
            e.printStackTrace();
            Map<String, String> body = ResponseBodyBuilder.createResponseBody( ResponseStatus.ERROR.name(), "Unexpected " +
                    "error while starting AVD Emulator: " + e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(body);
        }
    }

    @GetMapping("/stopAvd")
    public ResponseEntity<Map<String, String>> stopAvd() {
        try {
            this.avdEmulator.stopAvd();
            Map<String, String> body = ResponseBodyBuilder.createResponseBody( ResponseStatus.OK.name(),  "Successfully " +
                    "stopped AVD Emulator.");
            return ResponseEntity.status(HttpStatus.OK).body(body);
        } catch (Exception e) {
            e.printStackTrace();
            Map<String, String> body = ResponseBodyBuilder.createResponseBody( ResponseStatus.ERROR.name(), "Unexpected " +
                    "error while stopping AVD Emulator: " + e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(body);
        }
    }

    @GetMapping("/status")
    public ResponseEntity<Map<String, String>> checkStatus() {
        String status = this.avdEmulator.isProcessRunning() ? "Running" : "Stopped";
        Map<String, String> body = ResponseBodyBuilder.createResponseBodyWithMessage(ResponseStatus.OK.name(), "Emulator " +
                "is "+ status, status);
        return ResponseEntity.status(HttpStatus.OK).body(body);
    }


}
