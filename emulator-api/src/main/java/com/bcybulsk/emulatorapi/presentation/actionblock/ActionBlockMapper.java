package com.bcybulsk.emulatorapi.presentation.actionblock;

import com.bcybulsk.emulatorapi.model.actionblock.ActionBlock;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class ActionBlockMapper {
    public ActionBlock toActionBlock(ActionBlockDto actionBlockDto) {
        return ActionBlock.builder()
                .id(actionBlockDto.getId())
                .commandAction(actionBlockDto.getCommandAction())
                .subCommand1(actionBlockDto.getSubCommand1())
                .subCommand2(actionBlockDto.getSubCommand2())
                .subCommand3(actionBlockDto.getSubCommand3())
                .sleepSeconds(actionBlockDto.getSleepSeconds())
                .type(String.valueOf(actionBlockDto.getType()))
                .nextAction(actionBlockDto.getNextAction() != null ? toActionBlock(actionBlockDto.getNextAction()) :
                        null)
                .build();
    }

    public ActionBlockDto toActionBlockDto(ActionBlock actionBlock) {
        return ActionBlockDto.builder()
                .id(actionBlock.getId())
                .commandAction(actionBlock.getCommandAction())
                .sleepSeconds(actionBlock.getSleepSeconds())
                .subCommand1(actionBlock.getSubCommand1())
                .subCommand2(actionBlock.getSubCommand2())
                .subCommand3(actionBlock.getSubCommand3())
                .type(ActionBlock.ActionType.valueOf(actionBlock.getType()))
                .nextActionId(actionBlock.getNextAction() != null ? actionBlock.getNextAction().getId() : null)
//                .nextAction(actionBlock.getNextAction() != null ? toActionBlockDto(actionBlock.getNextAction()) : null)
                .build();
    }
}
