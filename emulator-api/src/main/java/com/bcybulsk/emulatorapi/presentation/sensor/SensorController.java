package com.bcybulsk.emulatorapi.presentation.sensor;

import com.bcybulsk.emulatorapi.model.sensor.Sensor;
import com.bcybulsk.emulatorapi.model.sensor.SensorService;
import com.bcybulsk.emulatorapi.system.Endpoint;
import com.bcybulsk.emulatorapi.telnetclient.AvdTelnetClientService;
import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequiredArgsConstructor
@RequestMapping(SensorController.SENSOR_URI)
public class SensorController {

    private static final Logger _log = LogManager.getLogger(SensorController.class);
    static final String SENSOR_URI = Endpoint.API_ROOT + "/sensor";

    private final AvdTelnetClientService avdTelnetClientService;
    private final SensorService sensorService;
    private final SensorMapper sensorMapper;
    private final SensorValidator validator;

    @GetMapping("/getSensors")
    public ResponseEntity<List<SensorDto>> getSensors() throws IOException {
        //todo check telnet connection
        for(Sensor.SensorName sensorName: Sensor.SensorName.values()) {
            SensorDto sensor = avdTelnetClientService.checkSensor(sensorName);
            sensorService.update(sensor);
        }
        return ResponseEntity.ok(
                sensorService.findAll().stream()
                .map(sensorMapper::toSensorDto)
                .collect(Collectors.toList())
        );
    }

    @PostMapping(consumes = {"multipart/form-data"})
    void addSensor(@ModelAttribute SensorDto sensor, BindingResult bindingResult) {
        validator.validate(sensor, bindingResult);

//        if(sensor.getId() != null) {
//            sensor = sensorService.findById(sensor.getId());
//        }

        sensorService.add(sensorMapper.toSensor(sensor));
    }
}
