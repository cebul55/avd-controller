package com.bcybulsk.emulatorapi.presentation.actionblock;

import com.bcybulsk.emulatorapi.model.actionblock.ActionBlock;
import lombok.*;

@Builder
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class ActionBlockDto {
    private Long id;
    private String commandAction;
    private String subCommand1;
    private String subCommand2;
    private String subCommand3;
    private int sleepSeconds;
    private ActionBlockDto nextAction;
    private Long nextActionId;
    private ActionBlock.ActionType type;
}
