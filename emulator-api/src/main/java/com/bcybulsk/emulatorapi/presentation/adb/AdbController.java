package com.bcybulsk.emulatorapi.presentation.adb;

import com.android.ddmlib.AndroidDebugBridge;
import com.android.ddmlib.IDevice;
import com.android.ddmlib.InstallException;
import com.bcybulsk.emulatorapi.model.adb.AdbPackage;
import com.bcybulsk.emulatorapi.model.adb.AdbService;
import com.bcybulsk.emulatorapi.model.error.TechnicalException;
import com.bcybulsk.emulatorapi.presentation.adb.device.DeviceDto;
import com.bcybulsk.emulatorapi.presentation.adb.device.DeviceMapper;
import com.bcybulsk.emulatorapi.presentation.storage.StorageService;
import com.bcybulsk.emulatorapi.system.Endpoint;
import com.bcybulsk.emulatorapi.system.properties.AdbProperties;
import com.bcybulsk.emulatorapi.telnetclient.ResponseStatus;
import com.bcybulsk.emulatorapi.util.ResponseBodyBuilder;
import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequiredArgsConstructor
@RequestMapping(AdbController.ADB_URI)
public class AdbController {

    private static final Logger _log = LogManager.getLogger(AdbController.class);
    static final String ADB_URI = Endpoint.API_ROOT + "/adb";
    private final AdbProperties adbProperties;
    private final AdbService adbService;
    private final DeviceMapper deviceMapper;

    private final StorageService storageService;

    @PostConstruct
    private void init() {}

    @PreDestroy
    private void deinit() {

    }

    @GetMapping("/")
    public ResponseEntity<List<DeviceDto>> getDevices() {
        if(!adbService.isAdbConnected()) {
            throw new TechnicalException("adb.service-is-not-connected");
        }
        List<IDevice> devices = adbService.getDeviceList();
        return ResponseEntity.ok(
                devices.stream()
                .map(deviceMapper::toDeviceDto)
                .collect(Collectors.toList())
        );
    }

    @GetMapping("/{deviceId}/installedPackages")
    public ResponseEntity<List<AdbPackage>> getInstalledPackages(@PathVariable String deviceId){
        List<AdbPackage> installedPackages = adbService.getInstalledPackages(deviceId);

        return ResponseEntity.ok(installedPackages);
    }

    @PostMapping("/{deviceId}/runPackage")
    public ResponseEntity<Map<String, String>> runPackage(@PathVariable String deviceId,
                                                          @RequestParam String activityName) {
        try {
            adbService.runPackage(deviceId, activityName);
        } catch (TechnicalException technicalException) {
            throw technicalException;
        }
        catch (Exception e) {
            e.printStackTrace();
            throw new TechnicalException("adb.unable-to-start-package", new String[] {e.getMessage()});
        }
        Map<String, String> body = ResponseBodyBuilder.createResponseBody(ResponseStatus.OK.name(), "Application " +
                "started");
        return ResponseEntity.ok(body);
    }

    @GetMapping("/{deviceId}/currentActivity")
    public ResponseEntity<Map<String, String>> getCurrentActivity(@PathVariable String deviceId) {
        String currentActivity = null;
        try {
            currentActivity = adbService.getCurrentActivity(deviceId);
        } catch (TechnicalException technicalException) {
            throw technicalException;
        } catch (Exception e) {
            e.printStackTrace();
            throw new TechnicalException("adb.unexpected-error", new String[] {e.getMessage()});
        }
// TODO beautify response
        Map<String, String> body = ResponseBodyBuilder.createResponseBodyWithMessage(ResponseStatus.OK.name(), "OK",
                currentActivity);
        return ResponseEntity.ok(body);
    }

    @PostMapping("/{deviceId}/stopActivity")
    public ResponseEntity<Map<String, String>> stopActivity(@PathVariable String deviceId,
                                                            @RequestParam String activityName){
        try {
            adbService.stopPackage(deviceId, activityName);
        } catch (TechnicalException technicalException) {
            throw technicalException;
        } catch (Exception e) {
            e.printStackTrace();
            throw new TechnicalException("adb.unable-to-stop-package", new String[] {e.getMessage()});
        }
        Map<String, String> body = ResponseBodyBuilder.createResponseBody(ResponseStatus.OK.name(), "Application " +
                "stopped");
        return ResponseEntity.ok(body);
    }

    @PostMapping("/{deviceId}/installPackage")
    public ResponseEntity<Map<String, String>> installPackage(@PathVariable String deviceId,
                                                              @RequestParam("apkFile") MultipartFile apkFile){
        String filename = apkFile.getOriginalFilename();
        if(!filename.contains(".apk")) {
            throw new TechnicalException("adb.file-is-not-package");
        }
        storageService.store(apkFile);

        Resource file = storageService.loadAsResource(filename);
        try {
            File f = file.getFile();
            String apkPath = f.getAbsolutePath();
            adbService.installPackage(deviceId, apkPath);
        } catch (TechnicalException technicalException) {
            throw technicalException;
        } catch (Exception e) {
            _log.error(e.getMessage(), e.fillInStackTrace());
            throw new TechnicalException("adb.unexpected-error", new String[] {e.getMessage()});
        }
        Map<String, String> body = ResponseBodyBuilder.createResponseBody(ResponseStatus.OK.name(), "Successfully " +
                "installed apk: " + filename);
        return ResponseEntity.ok(body);
    }

//    @DeleteMapping("/{deviceId}/uninstallPackage")
    @GetMapping("/{deviceId}/uninstallPackage")
    public ResponseEntity<Map<String, String>> uninstallPackage(@PathVariable String deviceId,
                                                                @RequestParam String packageName) {
        try {
            adbService.uninstallPackage(deviceId, packageName);
        } catch (InstallException e) {
            _log.error(e.getMessage(), e.fillInStackTrace());
            throw new TechnicalException("adb.unexpected-error", new String[] {e.getMessage()});
        }
        Map<String, String> body = ResponseBodyBuilder.createResponseBody(ResponseStatus.OK.name(), "Successfully " +
                "uninstalled apk: " + packageName);
        return ResponseEntity.ok(body);
    }
}
