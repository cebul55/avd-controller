package com.bcybulsk.emulatorapi.presentation.devicestate;

import com.bcybulsk.emulatorapi.model.devicestate.Detector;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class DetectorMapper {
    public DetectorDto toDetectorDto(Detector detector) {
        return DetectorDto.builder()
                .isFall(detector.isFall())
                .startFallTime(detector.getStartFallTime())
                .endFallTIme(detector.getEndFallTIme())
                .isLaying(detector.isLaying())
                .startLayingTime(detector.getStartLayingTime())
                .endLayingTime(detector.getEndLayingTime())
                .isPatternDetected(detector.isPatternDetected())
                .packageChangeDates(detector.getPackageChangeDates())
                .packageNamesList(detector.getPackageNamesList())
                .initialBatteryLevel(detector.getInitialBatteryLevel())
                .startObservationTime(detector.getStartObservationTime())
                .fastDischarge(detector.isFastDischarge())
                .highCpu(detector.isHighCpu())
                .batteryStatus(detector.getBatteryStatus())
                .build();
    }
}
