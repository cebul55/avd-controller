package com.bcybulsk.emulatorapi.presentation.devicestate;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@Builder
public class DetectorDto {
    private boolean isFall;
    private LocalDateTime startFallTime;
    private LocalDateTime endFallTIme;
    private boolean isLaying;
    private LocalDateTime startLayingTime;
    private LocalDateTime endLayingTime;
    private boolean isPatternDetected;
    private String[] packageNamesList;
    private LocalDateTime[] packageChangeDates;
    private int initialBatteryLevel;
    private LocalDateTime startObservationTime;
    private boolean fastDischarge;
    private boolean highCpu;
    private String batteryStatus;
}
