package com.bcybulsk.emulatorapi.presentation.storage;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@PropertySource(ignoreResourceNotFound = true,
        value = "classpath:storage.properties")
@Data
public class StorageProperties {

    /**
     * Folder location for storing files
     */
    @Value("${storage.dir}")
    private String location;

}