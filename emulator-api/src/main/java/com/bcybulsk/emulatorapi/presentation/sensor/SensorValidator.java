package com.bcybulsk.emulatorapi.presentation.sensor;

import com.bcybulsk.emulatorapi.model.error.TechnicalException;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class SensorValidator implements Validator {

    @Override
    public boolean supports(Class<?> aClass) {
        return aClass.isAssignableFrom(SensorDto.class);
    }

    @Override
    public void validate(Object target, Errors errors) {
        SensorDto model = (SensorDto) target;

        if(String.valueOf(model.getName()).isBlank()) {
            throw new TechnicalException("validation.sensorDto.name-isBlank");
        }
    }
}
