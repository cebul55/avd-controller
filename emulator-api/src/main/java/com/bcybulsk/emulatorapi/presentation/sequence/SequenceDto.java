package com.bcybulsk.emulatorapi.presentation.sequence;

import com.bcybulsk.emulatorapi.model.actionblock.ActionBlock;
import com.bcybulsk.emulatorapi.model.sequence.Sequence;
import com.bcybulsk.emulatorapi.presentation.actionblock.ActionBlockDto;
import lombok.*;

import java.time.LocalDateTime;
import java.util.List;

@Builder
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class SequenceDto {
    private Long id;
    private String name;
    private int numberOfBlocks;
    private Sequence.SequenceStatus status;
    private String exceptionMessage;
    private ActionBlockDto head;
    // property used to present list of actions
    private List<ActionBlockDto> actionBlockDtoList;
    private String owner;
    private LocalDateTime startTime;
    private LocalDateTime finishTime;
}
