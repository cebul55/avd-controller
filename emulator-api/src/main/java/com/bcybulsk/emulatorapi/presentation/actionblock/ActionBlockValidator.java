package com.bcybulsk.emulatorapi.presentation.actionblock;

import com.bcybulsk.emulatorapi.model.error.TechnicalException;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class ActionBlockValidator implements Validator {


    @Override
    public boolean supports(Class<?> aClass) {
        return aClass.isAssignableFrom(ActionBlockDto.class);
    }

    @Override
    public void validate(Object target, Errors errors) {
        ActionBlockDto model = (ActionBlockDto) target;

        if(String.valueOf(model.getCommandAction()).isBlank()) {
            throw new TechnicalException("validation.actionBlockDto.commandAction-isBlank");
        }
    }
}
