package com.bcybulsk.emulatorapi.presentation.adb.device;

import lombok.*;

@Builder
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class DeviceDto {
    private String avdName;
    private String serialNumber;
    private String state;
    private boolean isBootLoader;
    private boolean isEmulator;
    private boolean isOffline;
    private boolean isOnline;
}
