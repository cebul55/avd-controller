package com.bcybulsk.emulatorapi.presentation.sequence;

import com.bcybulsk.emulatorapi.model.actionblock.ActionBlock;
import com.bcybulsk.emulatorapi.model.actionblock.ActionBlockService;
import com.bcybulsk.emulatorapi.model.devicestate.DeviceStateService;
import com.bcybulsk.emulatorapi.model.sequence.Sequence;
import com.bcybulsk.emulatorapi.model.sequence.SequenceService;
import com.bcybulsk.emulatorapi.presentation.actionblock.ActionBlockDto;
import com.bcybulsk.emulatorapi.presentation.actionblock.ActionBlockMapper;
import com.bcybulsk.emulatorapi.system.Endpoint;
import com.bcybulsk.emulatorapi.system.async.FutureMap;
import com.bcybulsk.emulatorapi.telnetclient.ResponseStatus;
import com.bcybulsk.emulatorapi.util.ResponseBodyBuilder;
import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.*;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executor;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

@RestController
@RequiredArgsConstructor
@RequestMapping(SequenceController.SEQUENCE_URI)
public class SequenceController {

    private static final Logger _log = LogManager.getLogger(SequenceController.class);
    static final String SEQUENCE_URI = Endpoint.API_ROOT + "/sequence";
    private final SequenceService sequenceService;
    private final SequenceMapper sequenceMapper;
    private final SequenceValidator validator;
    private final ActionBlockService actionBlockService;
    private final ActionBlockMapper actionBlockMapper;
    private final FutureMap futureMap;
    private final DeviceStateService deviceStateService;

    private List<ActionBlockDto> getNextActionsDtoList(ActionBlock head) {
        List<ActionBlockDto> actionBlockDtoList = new ArrayList<>();
        ActionBlock actionBlock = head;
        do {
            actionBlockDtoList.add(actionBlockMapper.toActionBlockDto(actionBlock));
            actionBlock = actionBlock.getNextAction();
        } while (actionBlock != null);
        return actionBlockDtoList;
    }

    @GetMapping("/")
    public ResponseEntity<List<SequenceDto>> getSequences() {
        return ResponseEntity.ok(
                sequenceService.findAll().stream()
                .map(element -> {
                    SequenceDto sequenceDto = sequenceMapper.toSequenceDto(element);
                    if(element.getHead() != null)
                        sequenceDto.setActionBlockDtoList(getNextActionsDtoList(element.getHead()));

                    return sequenceDto;
                })
                .collect(Collectors.toList())
        );
 }

    @GetMapping("/{id}")
    public ResponseEntity<SequenceDto> getSequence(@PathVariable Long id) {
        Sequence sequence = sequenceService.findById(id);
        SequenceDto sequenceDto = sequenceMapper.toSequenceDto(sequence);
        if(sequence.getHead() != null)
            sequenceDto.setActionBlockDtoList(getNextActionsDtoList(sequence.getHead()));
        return ResponseEntity.ok(
            sequenceDto
        );
    }

    @PostMapping("/add")
    public ResponseEntity<SequenceDto> add(@RequestBody SequenceDto sequenceDto, BindingResult bindingResult) {
        validator.validate(sequenceDto, bindingResult);
        sequenceDto.setOwner("default");

        Sequence sequence = sequenceService.add(sequenceMapper.toSequence(sequenceDto));
        SequenceDto returnSequenceDto = sequenceMapper.toSequenceDto(sequence);
        if(sequence.getHead() != null)
            returnSequenceDto.setActionBlockDtoList(getNextActionsDtoList(sequence.getHead()));
        return ResponseEntity.ok(returnSequenceDto);
    }

    @PostMapping("/{id}/addNextAction")
    public ResponseEntity<SequenceDto> addNextAction(@PathVariable Long id,
                                                     @RequestBody ActionBlockDto actionBlockDto) {
        Sequence sequence = sequenceService.findById(id);
        ActionBlock actionBlock;
        if (actionBlockDto.getId() != null )
            actionBlock = actionBlockService.findById(actionBlockDto.getId());
        else
            actionBlock = actionBlockService.save(actionBlockMapper.toActionBlock(actionBlockDto));

        sequence = sequenceService.addNextActionBlock(sequence, actionBlock);
        return ResponseEntity.ok(sequenceMapper.toSequenceDto(sequence));
    }

    @PostMapping("/{id}/addHead")
    public ResponseEntity<SequenceDto> addHead(@PathVariable Long id,
                                                     @RequestBody ActionBlockDto actionBlockDto) {
        Sequence sequence = sequenceService.findById(id);
        ActionBlock actionBlock;
        if (actionBlockDto.getId() != null )
            actionBlock = actionBlockService.findById(actionBlockDto.getId());
        else
            actionBlock = actionBlockService.save(actionBlockMapper.toActionBlock(actionBlockDto));

        sequence = sequenceService.addHead(sequence, actionBlock);
        return ResponseEntity.ok(sequenceMapper.toSequenceDto(sequence));
    }

    @PostMapping("/{id}/updateActionList")
    public ResponseEntity<SequenceDto> updateActionList(@PathVariable Long id,
                                               @RequestBody String[] actionBlockIds) {
        Sequence sequence = sequenceService.findById(id);
        sequence = sequenceService.updateActionList(sequence, actionBlockIds);
        return ResponseEntity.ok(sequenceMapper.toSequenceDto(sequence));
    }
//    @DeleteMapping("/{id}/deleteAllActions")
    @GetMapping("/{id}/deleteAllActions")
    public ResponseEntity<Map<String, String>> deleteAllActions(@PathVariable Long id) {
        Sequence sequence = sequenceService.findById(id);
        sequenceService.deleteAllActions(sequence);
        Map<String, String> body = ResponseBodyBuilder.createResponseBody( ResponseStatus.OK.name(),
                "Successfully cleared action sequence.");
        return ResponseEntity.status(HttpStatus.OK).body(body);
    }

    @GetMapping("/{id}/deleteSequence")
    public ResponseEntity<Map<String, String>> deleteSequence(@PathVariable Long id) {
        Sequence sequence = sequenceService.findById(id);
        sequenceService.deleteSequence(sequence);
        Map<String, String> body = ResponseBodyBuilder.createResponseBody( ResponseStatus.OK.name(),
                "Successfully deleted sequence.");
        return ResponseEntity.status(HttpStatus.OK).body(body);
    }

    @GetMapping("/{id}/run")
    public SequenceDto runSequence(@PathVariable Long id) {
        Sequence sequence = sequenceService.findById(id);
        sequence = sequenceService.prepareToRunSequence(sequence);
        futureMap.putSequenceFuture(sequence.getId(),sequenceService.runSequence(sequence));
//        sequenceService.runSequence(sequence);
//        Map<String, String> body = ResponseBodyBuilder.createResponseBody( ResponseStatus.OK.name(),
//                "Successfully started action sequence.");

        return sequenceMapper.toSequenceDto(sequence);
    }

//    @DeleteMapping("/{id}/stop")
    @GetMapping("/{id}/stop")
    public void stopSequence(@PathVariable Long id) {
        Sequence sequence = sequenceService.findById(id);
        Future taskToStop = futureMap.getSequenceFuture(id);
        if(taskToStop != null)
            taskToStop.cancel(true);
//        return sequenceMapper.toSequenceDto(sequence);
    }

    @GetMapping("/{id}/status")
    public ResponseEntity<String> getStatus(@PathVariable Long id) {
        Sequence sequence = sequenceService.findById(id);
        String body = sequence.getStatus();
        return ResponseEntity.status(HttpStatus.OK).body(body);
    }

}
