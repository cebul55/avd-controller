package com.bcybulsk.emulatorapi.presentation.error;

public enum ErrorType {
    TECHNICAL,
    SYSTEM,
    AUTH
}
