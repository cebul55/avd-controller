package com.bcybulsk.emulatorapi.presentation.devicestate;

import com.bcybulsk.emulatorapi.model.devicestate.DeviceState;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class DeviceStateMapper {
    private final DetectorMapper fallDetectorMapper;

    public DeviceStateDto toDeviceStateDto(DeviceState deviceState){
        return DeviceStateDto.builder()
                .deviceId(deviceState.getDeviceId())
                .isAlive(deviceState.isAlive())
                .cpuUsagePercentage(deviceState.getCpuUsagePercentage())
                .statusTime(deviceState.getStatusTime())
                .capacity(deviceState.getCapacity())
                .status(deviceState.getStatus())
                .health(deviceState.getHealth())
                .longitude(deviceState.getLongitude())
                .latitude(deviceState.getLatitude())
                .acceleration(deviceState.getAcceleration())
                .gyroscope(deviceState.getGyroscope())
                .magneticField(deviceState.getMagneticField())
                .orientation(deviceState.getOrientation())
                .temperature(deviceState.getTemperature())
                .proximity(deviceState.getProximity())
                .light(deviceState.getLight())
                .pressure(deviceState.getPressure())
                .humidity(deviceState.getHumidity())
                .magneticFieldUncalibrated(deviceState.getMagneticFieldUncalibrated())
                .gyroscopeUncalibrated(deviceState.getGyroscopeUncalibrated())
                .hingeAngle0(deviceState.getHingeAngle0())
                .hingeAngle1(deviceState.getHingeAngle1())
                .hingeAngle2(deviceState.getHingeAngle2())
                .heartRate(deviceState.getHeartRate())
                .detectorDto(fallDetectorMapper.toDetectorDto(deviceState.getDetector()))
                .build();
    }
}
