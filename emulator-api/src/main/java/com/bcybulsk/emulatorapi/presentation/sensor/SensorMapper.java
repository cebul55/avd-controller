package com.bcybulsk.emulatorapi.presentation.sensor;

import com.bcybulsk.emulatorapi.model.sensor.Sensor;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class SensorMapper {

    public Sensor toSensor(SensorDto sensorDto) {
        return Sensor.builder()
                .id(sensorDto.getId())
                .name(String.valueOf(sensorDto.getName()))
                .status(String.valueOf(sensorDto.getStatus()))
                .value(sensorDto.getValue())
                .build();
    }

    public SensorDto toSensorDto(Sensor sensor) {
        return SensorDto.builder()
                .id(sensor.getId())
                .name(Sensor.SensorName.valueOf(sensor.getName()))
                .status(Sensor.SensorState.valueOf(sensor.getStatus()))
                .value(sensor.getValue())
                .build();
    }
}
