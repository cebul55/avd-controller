package com.bcybulsk.emulatorapi.presentation.sequence;

import com.bcybulsk.emulatorapi.model.error.TechnicalException;
import com.bcybulsk.emulatorapi.model.sequence.SequenceService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
@RequiredArgsConstructor
public class SequenceValidator implements Validator {

    private final SequenceService sequenceService;

    @Override
    public boolean supports(Class<?> aClass) {
        return aClass.isAssignableFrom(SequenceDto.class);
    }

    @Override
    public void validate(Object target, Errors errors) {
        SequenceDto model = (SequenceDto) target;

        if(String.valueOf(model.getName()).isBlank()) {
            throw new TechnicalException("validation.sequenceDto.name-isBlank");
        }

        if(sequenceService.findByName(model.getName()) != null) {
            throw new TechnicalException("validation.sequenceDto.name-isNotUnique");
        }

        if(model.getNumberOfBlocks() != 0) {
            throw new TechnicalException("validation.sequenceDto.numberOfBlocks-initialZero");
        }
    }
}
