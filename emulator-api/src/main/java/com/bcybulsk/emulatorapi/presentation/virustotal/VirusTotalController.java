package com.bcybulsk.emulatorapi.presentation.virustotal;

import com.bcybulsk.emulatorapi.model.error.TechnicalException;
import com.bcybulsk.emulatorapi.presentation.storage.StorageService;
import com.bcybulsk.emulatorapi.system.Endpoint;
import com.bcybulsk.emulatorapi.system.properties.ExternalApiProperties;
import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.core.io.Resource;
import org.springframework.http.*;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.*;

@RestController
@RequiredArgsConstructor
@RequestMapping(VirusTotalController.VT_URI)
public class VirusTotalController {
    private static final Logger _log = LogManager.getLogger(VirusTotalController.class);
    static final String VT_URI = Endpoint.API_ROOT + "/virustotal";
    private final RestTemplate restTemplate;

    private final ExternalApiProperties externalApiProperties;
    private final StorageService storageService;

//    @Value("${vt.apiAddress}")
//    private final VirusTotalProperties vtApiBaseUri;
//
//    @Value("${vt.apiKey}")
//    private final String vtApiKey;

    @GetMapping("/file/{sha}")
    public ResponseEntity<Object> getFileBySha(@PathVariable String sha) {

        //5badfe9e878e6a122b08c4d07c9f6d2b977031949e90ad6ee1704ee0ede91587
        String uri = externalApiProperties.getVtApiAddress() + "/files/" + sha;

        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.add("x-apiKey", externalApiProperties.getVtApiKey());

        HttpEntity<String> entity = new HttpEntity<>("body", headers);
        return restTemplate.exchange(uri,
                HttpMethod.GET, entity, Object.class);
    }

    @PostMapping("/file")
    public ResponseEntity<Object> uploadAndAnalseFile(@RequestParam("file") MultipartFile file) throws IOException {

        // 1. getting upload url
        String uri = externalApiProperties.getVtApiAddress() + "/files/upload_url";

        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.add("x-apiKey", externalApiProperties.getVtApiKey());
        HttpEntity<String> entity = new HttpEntity<>("body", headers);
        ResponseEntity<Map> response = restTemplate.exchange(uri, HttpMethod.GET, entity,
                Map.class);

        if(!response.getBody().containsKey("data"))
            throw new TechnicalException("vt.unable-to-obtain-upload-url");
        String upload_uri = (String) response.getBody().get("data");
        _log.info("Upload uri virus total files=" + upload_uri);

        // 2. upload file to obtained url
        String filename = file.getOriginalFilename();
        storageService.store(file);
        Resource savedFile = storageService.loadAsResource(filename);

        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
        body.add("file", savedFile.getFile().getAbsolutePath());
//        body.add("file", savedFile.getFile());
//        body.add("filename", savedFile.getFilename());
////        Map<String, File> body = new HashMap<String, File>();
////        body.put("file", savedFile.getFile());
//
        HttpEntity<MultiValueMap<String, Object>> postEntity = new HttpEntity<>(body, headers);
        _log.debug(entity.toString());
        ResponseEntity<Object> responsePost = restTemplate.exchange(upload_uri, HttpMethod.POST, postEntity, Object.class);
        return responsePost;
    }
}
