package com.bcybulsk.emulatorapi.presentation.storage;

import com.bcybulsk.emulatorapi.model.error.TechnicalException;
import com.bcybulsk.emulatorapi.system.Endpoint;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.core.io.Resource;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequiredArgsConstructor
@RequestMapping(StorageController.STORAGE_URI)
public class StorageController {

    static final String STORAGE_URI = Endpoint.API_ROOT + "/filestorage"; ;
    private final StorageService storageService;
    @Value("${avd.error_log}")
    private String avdErrorFilePath;

    @Value("${avd.output_log}")
    private String avdOutputFilePath;

    @Value("${avd.logcatPath")
    private String logcatFilePath;

    @GetMapping("/")
    public List<String> listUploadedFiles() {
        return storageService.loadAll().map(
                path -> MvcUriComponentsBuilder
                        .fromMethodName(StorageController.class, "serveFile", path.getFileName().toString()).build().toUri().toString())
                        .collect(Collectors.toList());
    }

    @GetMapping("/{filename:.+}")
    @ResponseBody
    public ResponseEntity<Resource> serveFile(@PathVariable String filename){
        Resource file = storageService.loadAsResource(filename);
        return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION,
                "attachment; filename=\"" + file.getFilename() + "\"").body(file);
    }

    @PostMapping("/")
    public String handleFileUpload(@RequestParam("file") MultipartFile file, RedirectAttributes redirectAttributes) {
        storageService.store(file);
        redirectAttributes.addFlashAttribute("message",
                "You successfully uploaded " + file.getOriginalFilename() + "!");

        return "redirect:/";
    }

    @GetMapping("/logs/{type}")
    public ResponseEntity<Resource> serveLogFile(@PathVariable String type) {
        Resource file = null;
        switch (type) {
            case "avd_log": {
                file = storageService.loadAsResourceFromAbsolutePath(avdOutputFilePath);
                break;
            }
            case "avd_error": {
                file = storageService.loadAsResourceFromAbsolutePath(avdErrorFilePath);
                break;
            }
            case "logcat": {
                file = storageService.loadAsResourceFromAbsolutePath(logcatFilePath);
                break;
            }
            default:
                throw new TechnicalException("error");
        }
        return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION,
                "attachment; filename=\"" + file.getFilename() + "\"").body(file);

    }
}
