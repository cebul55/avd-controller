package com.bcybulsk.emulatorapi.presentation.devicestate;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@Builder
public class DeviceStateDto {
    private String deviceId;
    private boolean isAlive;
    private List<Double> cpuUsagePercentage;
    private List<LocalDateTime> statusTime;
    private List<Integer> capacity;
    private List<String> status;
    private List<String> health;
    private List<Double> longitude;
    private List<Double> latitude;
    private List<String> acceleration;
    private List<String> gyroscope;
    private List<String> magneticField;
    private List<String> orientation;
    private List<String> temperature;
    private List<String> proximity;
    private List<String> light;
    private List<String> pressure;
    private List<String> humidity;
    private List<String> magneticFieldUncalibrated;
    private List<String> gyroscopeUncalibrated;
    private List<String> hingeAngle0;
    private List<String> hingeAngle1;
    private List<String> hingeAngle2;
    private List<String> heartRate;
    private DetectorDto detectorDto;

}
