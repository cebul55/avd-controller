package com.bcybulsk.emulatorapi.presentation.sensor;

import com.bcybulsk.emulatorapi.model.sensor.Sensor;
import lombok.*;

@Builder
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class SensorDto {
    private Long id;
    private Sensor.SensorName name;
    private Sensor.SensorState status;
    private String value;
}
