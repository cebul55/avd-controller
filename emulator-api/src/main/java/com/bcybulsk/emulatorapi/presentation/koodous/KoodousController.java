package com.bcybulsk.emulatorapi.presentation.koodous;

import com.bcybulsk.emulatorapi.system.Endpoint;
import com.bcybulsk.emulatorapi.system.properties.ExternalApiProperties;
import lombok.RequiredArgsConstructor;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;

@RestController
@RequiredArgsConstructor
@RequestMapping(KoodousController.KOODOUS_URI)
public class KoodousController {
    public static final String KOODOUS_URI = Endpoint.API_ROOT + "/koodous";
    private final RestTemplate restTemplate;

    private final ExternalApiProperties externalApiProperties;

    @GetMapping("/analysis/{sha}")
    public ResponseEntity<Object> getApkAnalysis(@PathVariable String sha) {
        String uri = externalApiProperties.getKoodousApiAddress() + "/apks/" + sha + "/analysis";
        String apiToken = "Token " + externalApiProperties.getKoodousApiKey();

        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.add("Authorization", apiToken);

        HttpEntity<String> entity = new HttpEntity<>("body", headers);
        return restTemplate.exchange(uri,
                HttpMethod.GET, entity, Object.class);
    }

    @GetMapping("/votes/{sha}")
    public ResponseEntity<Object> getApkFeed(@PathVariable String sha) {
        String uri = externalApiProperties.getKoodousApiAddress() + "/apks/" + sha + "/votes";

        HttpEntity<String> entity = createKoodousRequestEntity();
        return restTemplate.exchange(uri,
                HttpMethod.GET, entity, Object.class);
    }

//    @GetMapping("/kind/{sha}")
//    public ResponseEntity<Object> getKind(@PathVariable String sha) {
//        String uri = externalApiProperties.getKoodousApiAddress() + "/apks/" + sha + "/votes";
//
//        HttpEntity<String> entity = createKoodousRequestEntity();
//        return restTemplate.exchange(uri,
//                HttpMethod.POST, entity, Object.class);
//    }

    private HttpEntity<String> createKoodousRequestEntity() {
        String apiToken = "Token " + externalApiProperties.getKoodousApiKey();

        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.add("Authorization", apiToken);
//        headers.setContentType(MediaType.APPLICATION_JSON);
//todo fix: https://stackoverflow.com/questions/67128564/virustotal-upload-file
        HttpEntity<String> entity = new HttpEntity<>("", headers);
        return entity;
    }

}

