package com.bcybulsk.emulatorapi.presentation.devicestate;

import com.bcybulsk.emulatorapi.model.adb.AdbService;
import com.bcybulsk.emulatorapi.model.devicestate.DeviceStateService;
import com.bcybulsk.emulatorapi.model.error.TechnicalException;
import com.bcybulsk.emulatorapi.system.Endpoint;
import com.bcybulsk.emulatorapi.system.async.FutureMap;
import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.concurrent.Future;

@RestController
@RequiredArgsConstructor
@RequestMapping(DeviceStateController.DEVICESTATE_URI)
public class DeviceStateController {
    private static final Logger _log = LogManager.getLogger(DeviceStateController.class);
    public static final String DEVICESTATE_URI = Endpoint.API_ROOT + "/devicestate" ;
    private final DeviceStateService deviceStateService;
    private final DeviceStateMapper deviceStateMapper;
    private final FutureMap futureMap;
    private final AdbService adbService;
    private final DetectorMapper detectorMapper;

    @GetMapping("/{deviceName}/monitor")
    public ResponseEntity<String> startDeviceMonitor(@PathVariable String deviceName) {
        if(adbService.getDeviceByName(deviceName) == null)
            throw new TechnicalException("adb.device-not-found", new String[] {deviceName});

        futureMap.putDeviceMonitorFuture(deviceName,
                deviceStateService.startDeviceStateMonitor(deviceName));
        return ResponseEntity.status(HttpStatus.OK).body("OK");
    }

    @GetMapping("/{deviceName}")
    public DeviceStateDto getDeviceMonitorData(@PathVariable String deviceName) {
        if(adbService.getDeviceByName(deviceName) == null)
            throw new TechnicalException("adb.device-not-found", new String[] {deviceName});
        return deviceStateMapper.toDeviceStateDto(deviceStateService.getDeviceState(deviceName));
    }

    @GetMapping("/{deviceName}/detector")
    public DetectorDto getDeviceDetectorData(@PathVariable String deviceName) {
        if(adbService.getDeviceByName(deviceName) == null)
            throw new TechnicalException("adb.device-not-found", new String[] {deviceName});
        return detectorMapper.toDetectorDto(deviceStateService.getDeviceDetectorState(deviceName));
    }

//    @DeleteMapping("/{deviceName}")
    @PostMapping("/{deviceName}")
    public void stopDeviceMonitor(@PathVariable String deviceName) {
        if(adbService.getDeviceByName(deviceName) == null)
            throw new TechnicalException("adb.device-not-found", new String[] {deviceName});
        Future taskToStop = null;
        taskToStop = futureMap.getDeviceMonitorFuture(deviceName);
        if(taskToStop != null) {
            while(taskToStop != null && !taskToStop.isCancelled()) {
                taskToStop = futureMap.getDeviceMonitorFuture(deviceName);
                taskToStop.cancel(true);
                futureMap.popDeviceMonitorFuture(deviceName);

            }
        }
    }
}
