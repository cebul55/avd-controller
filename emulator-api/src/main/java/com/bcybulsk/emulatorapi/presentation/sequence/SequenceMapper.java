package com.bcybulsk.emulatorapi.presentation.sequence;

import com.bcybulsk.emulatorapi.model.actionblock.ActionBlockEntityMapper;
import com.bcybulsk.emulatorapi.model.sequence.Sequence;
import com.bcybulsk.emulatorapi.presentation.actionblock.ActionBlockMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class SequenceMapper {

    private final ActionBlockMapper actionBlockMapper;

    public Sequence toSequence(SequenceDto sequenceDto) {
        return Sequence.builder()
                .id(sequenceDto.getId())
                .name(sequenceDto.getName())
                .numberOfBlocks(sequenceDto.getNumberOfBlocks())
                .status(String.valueOf(sequenceDto.getStatus()))
                .exceptionMessage(sequenceDto.getExceptionMessage())
                .owner(sequenceDto.getOwner())
                .startTime(sequenceDto.getStartTime())
                .finishTime(sequenceDto.getFinishTime())
                .head(sequenceDto.getHead() != null ? actionBlockMapper.toActionBlock(sequenceDto.getHead()) : null)
                .build();
    }

    public SequenceDto toSequenceDto(Sequence sequence) {
        return SequenceDto.builder()
                .id(sequence.getId())
                .name(sequence.getName())
                .numberOfBlocks(sequence.getNumberOfBlocks())
                .status(Sequence.SequenceStatus.valueOf(sequence.getStatus()))
                .exceptionMessage(sequence.getExceptionMessage())
                .owner(sequence.getOwner())
                .startTime(sequence.getStartTime())
                .finishTime(sequence.getFinishTime())
                .head(sequence.getHead() != null ? actionBlockMapper.toActionBlockDto(sequence.getHead()) : null)
                .build();
    }
}
