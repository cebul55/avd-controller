package com.bcybulsk.emulatorapi.presentation.actionblock;


import com.bcybulsk.emulatorapi.model.actionblock.ActionBlock;
import com.bcybulsk.emulatorapi.model.actionblock.ActionBlockService;
import com.bcybulsk.emulatorapi.system.Endpoint;
import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequiredArgsConstructor
@RequestMapping(ActionBlockController.ACTION_BLOCK_URI)
public class ActionBlockController {
    private static final Logger _log = LogManager.getLogger(ActionBlockController.class);
    public static final String ACTION_BLOCK_URI = Endpoint.API_ROOT + "/actionBlock";
    private final ActionBlockService actionBlockService;
    private final ActionBlockMapper actionBlockMapper;

    @GetMapping("/getActions")
    public ResponseEntity<List<ActionBlockDto>> getActions() {
        return ResponseEntity.ok(
                actionBlockService.findAll().stream()
                .map(actionBlockMapper::toActionBlockDto)
                .collect(Collectors.toList())
        );
    }

    @PostMapping("/add")
    public ResponseEntity<ActionBlockDto> addAction(@RequestBody ActionBlockDto actionBlockDto) {
        ActionBlock savedBlock = actionBlockService.save(actionBlockMapper.toActionBlock(actionBlockDto));
        return ResponseEntity.ok(actionBlockMapper.toActionBlockDto(savedBlock));
    }
// todo change mapping to delete - cors errors were occuring so temporary solution was provided
//    @DeleteMapping("/{id}")
    @PostMapping("/delete/{id}")
    public void deleteAction(@PathVariable Long id) {
        ActionBlock actionBlockToDelete = actionBlockService.findById(id);
        actionBlockService.delete(actionBlockToDelete);
    }

    @GetMapping("/{id}")
    public ResponseEntity<ActionBlockDto> getActionBlock(@PathVariable Long id) {
        ActionBlock actionBlock= actionBlockService.findById(id);
        return ResponseEntity.ok(actionBlockMapper.toActionBlockDto(actionBlock));
    }

    @PostMapping("/addNextAction/{id}")
    public ResponseEntity<ActionBlockDto> addNextAction(@PathVariable Long id,
                                                        @RequestBody ActionBlockDto childActionDto) {
        ActionBlock parent = actionBlockService.findById(id);
        ActionBlock child = actionBlockService.save(actionBlockMapper.toActionBlock(childActionDto));

        parent.setNextAction(child);
        return ResponseEntity.ok(actionBlockMapper.toActionBlockDto(actionBlockService.save(parent)));

    }

    @PostMapping("/update/{id}")
    public ResponseEntity<ActionBlockDto> updateAction(@PathVariable Long id,
                                                       @RequestBody ActionBlockDto actionBlockDto) {
        ActionBlock actionBlock = actionBlockService.findById(id);

        actionBlock.setCommandAction(actionBlockDto.getCommandAction());
        actionBlock.setSubCommand1(actionBlockDto.getSubCommand1());
        actionBlock.setSubCommand2(actionBlockDto.getSubCommand2());
        actionBlock.setSubCommand3(actionBlockDto.getSubCommand3());
        actionBlock.setType(String.valueOf(actionBlockDto.getType()));
        actionBlock.setSleepSeconds(actionBlockDto.getSleepSeconds());
//        if(actionBlockDto.getNextAction() != null)
//            actionBlock.setNextAction(actionBlockMapper.toActionBlock(actionBlockDto.getNextAction()));
//        else
//            actionBlock.setNextAction(null);

        actionBlock = actionBlockService.save(actionBlock);
        return ResponseEntity.ok(actionBlockMapper.toActionBlockDto(actionBlock));
    }

}
