package com.bcybulsk.emulatorapi.presentation.adb.device;

import com.android.ddmlib.IDevice;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class DeviceMapper {

    public DeviceDto toDeviceDto(IDevice iDevice) {
        return DeviceDto.builder()
                .avdName(iDevice.getAvdName())
                .serialNumber(iDevice.getSerialNumber())
                .state(iDevice.getState().name())
                .isBootLoader(iDevice.isBootLoader())
                .isEmulator(iDevice.isEmulator())
                .isOffline(iDevice.isOffline())
                .isOnline(iDevice.isOnline())
                .build();
    }
}
