package com.bcybulsk.emulatorapi.model.devicestate;

import com.bcybulsk.emulatorapi.model.adb.AdbService;
import com.bcybulsk.emulatorapi.model.error.TechnicalException;
import com.bcybulsk.emulatorapi.telnetclient.AvdTelnetClientService;
import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

@Service
@RequiredArgsConstructor
public class DeviceStateService {

    private final DeviceStateMap deviceStateMap;
    private static final Logger _log = LogManager.getLogger(DeviceStateService.class);
    private static final int SLEEP_MILISECONDS = 250;
    private static final String EMPTY_DATA = "0:0:0";
    private final AdbService adbService;

    @Value("${avd.device_name}")
    private String defaultName;
    private final AvdTelnetClientService avdTelnetClientService;

    @Async("threadDeviceMonitor")
    public Future startDeviceStateMonitor(String deviceName) {
        // todo remove default value
        if(deviceName.equals(""))
            deviceName = defaultName;
        DeviceState deviceState = deviceStateMap.getDeviceState(deviceName);
        try{

            while(!Thread.currentThread().isInterrupted()) {
                try {
                    // do stuff
                    deviceState.addStatusTime();
                    updateCpuData(deviceState);
                    updateBatteryStatus(deviceState);
                    updateLocation(deviceState);
                    updateCurrentPackage(deviceState);
                    updateSensor(deviceState, "acceleration");
                    updateSensor(deviceState, "gyroscope");
                    updateSensor(deviceState, "magnetic-field");
                    updateSensor(deviceState, "orientation");
                    updateSensor(deviceState, "temperature");
                    updateSensor(deviceState, "proximity");
                    updateSensor(deviceState, "light");
                    updateSensor(deviceState, "pressure");
                    updateSensor(deviceState, "humidity");
                    updateSensor(deviceState, "magnetic-field-uncalibrated");
                    updateSensor(deviceState, "gyroscope-uncalibrated");
                    updateSensor(deviceState, "hinge-angle0");
                    updateSensor(deviceState, "hinge-angle1");
                    updateSensor(deviceState, "hinge-angle2");
                    updateSensor(deviceState, "heart-rate");
                } catch (InterruptedException interruptedException) {
                    _log.debug("Interrupted execution of monitor for device=" + deviceState.getDeviceId());
                    break;
                } catch (TechnicalException technicalException) {
                    throw technicalException;
                } catch (Exception e) {
                    _log.error(e.getMessage(), e.fillInStackTrace());
                }
                TimeUnit.MILLISECONDS.sleep(SLEEP_MILISECONDS);

            }
        } catch (InterruptedException interruptedException) {
            _log.debug("Interrupted execution of monitor for device=" + deviceState.getDeviceId());
        }
        return null;
    }

    private void updateCurrentPackage(DeviceState deviceState) {
        try {
            String currentPackage = adbService.getCurrentActivity(deviceState.getDeviceId());
            deviceState.updateDetectorPackageSequence(currentPackage, LocalDateTime.now());
        } catch (Exception e) {
            _log.error(e.getMessage(), e.fillInStackTrace());
            e.printStackTrace();
        }
    }

    private void updateSensor(DeviceState deviceState, String sensorName) {
        try {
//            String telnetCommand = "sensor get acceleration";
            String telnetCommand = "sensor get " + sensorName;
            String[] response = avdTelnetClientService.sendCommandForResponse(telnetCommand);
            for(String line : response) {
                if(line.contains(sensorName)) {
                    String sensorValue = line.split(" = ")[1];
                    sensorValue = sensorValue.trim();
                    deviceState.addSensorValue(sensorName, sensorValue);
                }
            }
        } catch (Exception e) {
            _log.error(e.getMessage());
            deviceState.addSensorValue(sensorName, EMPTY_DATA);
        }
    }

    private void updateLocation(DeviceState deviceState) {
        try {
            Double[] location = adbService.getLocation(deviceState.getDeviceId());
            deviceState.addLocation(location[0], location[1]);
        } catch (Exception e) {
            _log.error(e.getMessage());
            deviceState.addLocation(0.0, 0.0);
        }
    }

    private void updateBatteryStatus(DeviceState deviceState) {
        try {
            String telnetCommand = "power display";
            String[] response = avdTelnetClientService.sendCommandForResponse(telnetCommand);
            for (String line : response) {
                if (line.contains("status")) {
                    String status = line.split(":")[1];
                    status = status.trim();
                    deviceState.addBatteryStatus(status);
                    deviceState.updateDetectorBatteryStatus(status);
                } else if (line.contains("health")) {
                    String health = line.split(":")[1];
                    health = health.trim();
                    deviceState.addBatteryHealth(health);
                } else if (line.contains("capacity")) {
                    String capacity = line.split(":")[1];
                    int capacityValue = Integer.parseInt(capacity.trim());
                    deviceState.addBatteryCapacity(capacityValue);
                    deviceState.updateDetectorBatteryLevel(capacityValue);
                }
            }
        } catch (Exception e) {
            _log.error(e.getMessage());
            deviceState.addBatteryCapacity(0);
            deviceState.addBatteryHealth("-");
            deviceState.addBatteryStatus("-");
        }
    }

    private void updateCpuData(DeviceState deviceState) throws Exception {
        double cpuUsage = adbService.getCpuInfo(deviceState.getDeviceId());
        deviceState.addCpuUsagePercentage(cpuUsage);
    }

    public DeviceState getDeviceState(String deviceName) {
        DeviceState deviceState = deviceStateMap.getDeviceState(deviceName);
        if(deviceState == null)
            throw new TechnicalException("device-state.not-exist", new String[]{ deviceName });
        return deviceState;
    }

    public Detector getDeviceDetectorState(String deviceName) {
        DeviceState deviceState = getDeviceState(deviceName);
        return deviceState.getDetector();
    }
}
