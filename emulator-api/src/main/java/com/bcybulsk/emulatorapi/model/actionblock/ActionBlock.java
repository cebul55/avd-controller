package com.bcybulsk.emulatorapi.model.actionblock;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Table;

@Builder
@Getter
@Setter
public class ActionBlock {
    Long id;
    private String commandAction;
    private String subCommand1;
    private String subCommand3;
    private String subCommand2;
    private int sleepSeconds;
    private ActionBlock nextAction;
    private String type;

    public enum ActionType {
        avdEmulator, adb, koodous, virustotal;
    }
}
