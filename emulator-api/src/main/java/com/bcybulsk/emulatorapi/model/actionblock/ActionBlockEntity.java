package com.bcybulsk.emulatorapi.model.actionblock;

import com.bcybulsk.emulatorapi.model.sequence.Sequence;
import com.bcybulsk.emulatorapi.model.sequence.SequenceEntity;
import lombok.*;

import javax.persistence.*;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "action_block")
public class ActionBlockEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "command_action", nullable = false, unique = false)
    private String commandAction;

    @Column(name = "subcommand_1", nullable = true, unique = false)
    private String subCommand1;

    @Column(name = "subcommand_2", nullable = true, unique = false)
    private String subCommand2;

    @Column(name = "subcommand_3", nullable = true, unique = false)
    private String subCommand3;

    @Column(name = "sleep", nullable = false, unique = false)
    private int sleepSeconds;

    @Enumerated(EnumType.STRING)
    @Column(name = "type", nullable = true, unique = false)
    private ActionBlock.ActionType type;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = false)
    @JoinColumn(name = "next_action", referencedColumnName = "id", nullable = true, unique = true)
    private ActionBlockEntity nextAction;

    @OneToOne(mappedBy = "head")
    private SequenceEntity sequenceEntity;

}
