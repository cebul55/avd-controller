package com.bcybulsk.emulatorapi.model.actionblock;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class ActionBlockEntityMapper {

    public ActionBlock toActionBlock(ActionBlockEntity actionBlockEntity) {
        return ActionBlock.builder()
                .id(actionBlockEntity.getId())
                .commandAction(actionBlockEntity.getCommandAction())
                .subCommand1(actionBlockEntity.getSubCommand1())
                .subCommand2(actionBlockEntity.getSubCommand2())
                .subCommand3(actionBlockEntity.getSubCommand3())
                .sleepSeconds(actionBlockEntity.getSleepSeconds())
                .type(String.valueOf(actionBlockEntity.getType()))
                .nextAction(actionBlockEntity.getNextAction() != null
                        && actionBlockEntity.getNextAction().getId() != null ?
                        toActionBlock(actionBlockEntity.getNextAction()) : null)
                .build();
    }

    public ActionBlockEntity toActionBlockEntity(ActionBlock actionBlock) {
        return ActionBlockEntity.builder()
                .id(actionBlock.getId())
                .commandAction(actionBlock.getCommandAction())
                .subCommand1(actionBlock.getSubCommand1())
                .subCommand2(actionBlock.getSubCommand2())
                .subCommand3(actionBlock.getSubCommand3())
                .sleepSeconds(actionBlock.getSleepSeconds())
                .type(ActionBlock.ActionType.valueOf(actionBlock.getType()))
                .nextAction(actionBlock.getNextAction() != null
                        && actionBlock.getNextAction().getId() != null ?
                        this.toActionBlockEntity(actionBlock.getNextAction()) : null)
                .build();
    }
}
