package com.bcybulsk.emulatorapi.model.actionblock;

import com.bcybulsk.emulatorapi.model.adb.AdbService;
import com.bcybulsk.emulatorapi.model.error.TechnicalException;
import com.bcybulsk.emulatorapi.model.sequence.SequenceEntity;
import com.bcybulsk.emulatorapi.model.sequence.SequenceRepository;
import com.bcybulsk.emulatorapi.telnetclient.AvdTelnetClientService;
import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ActionBlockService {

    private final ActionBlockEntityMapper actionBlockEntityMapper;
    private final ActionBlockRepository actionBlockRepository;
    private static final Logger _log = LogManager.getLogger(ActionBlockService.class);
    private final AvdTelnetClientService avdTelnetClientService;
    private final AdbService adbService;
    private final SequenceRepository sequenceRepository;


    @Transactional
    public List<ActionBlock> findAll() {
        return actionBlockRepository.findAll().stream()
                .map(actionBlockEntityMapper::toActionBlock)
                .collect(Collectors.toList());
    }

    @Transactional
    public ActionBlock save(ActionBlock actionBlock) {
        _log.info("Added action block with action: " + actionBlock.getCommandAction());
        if(actionBlock.getNextAction() != null){
            actionBlock.setNextAction(save(actionBlock.getNextAction()));
        }
        return actionBlockEntityMapper.toActionBlock(
                actionBlockRepository.save(actionBlockEntityMapper.toActionBlockEntity(actionBlock)));
    }

    @Transactional
    public void deleteAll() {
        actionBlockRepository.deleteAll();
        _log.info("Cleared action blocks data.");
    }

    @Transactional
    public ActionBlock findById(Long id) {
        return actionBlockRepository.findById(id)
                .map(actionBlockEntityMapper::toActionBlock)
                .orElseThrow(() -> new TechnicalException("error.actionblock-not-exists", new String[]{id.toString()}));

    }

    @Transactional
    public void delete(ActionBlock actionBlockToDelete) {

        ActionBlockEntity parentEntity =
                actionBlockRepository.findByNextAction(actionBlockEntityMapper.toActionBlockEntity(actionBlockToDelete));
        ActionBlock child = actionBlockToDelete.getNextAction();
        if(parentEntity == null && child == null) {
            _log.debug("Deleting sequence head. Changing to empty sequence");
            SequenceEntity sequenceEntity =
                    sequenceRepository.findByHead(actionBlockEntityMapper.toActionBlockEntity(actionBlockToDelete));
            if(sequenceEntity != null) {
                sequenceEntity.setHead(null);
                sequenceRepository.save(sequenceEntity);
            }
        } else if (parentEntity == null && child != null) {
            _log.debug("Deleting sequence head. Changing to sequence head to: " + child.getId());
            SequenceEntity sequenceEntity =
                    sequenceRepository.findByHead(actionBlockEntityMapper.toActionBlockEntity(actionBlockToDelete));
            sequenceEntity.setHead(actionBlockEntityMapper.toActionBlockEntity(child));
            ActionBlockEntity childEntity = actionBlockEntityMapper.toActionBlockEntity(child);
            childEntity.setSequenceEntity(sequenceEntity);
            actionBlockRepository.save(childEntity);
            sequenceRepository.save(sequenceEntity);
            _log.debug("Deleting sequence head. Changing to sequence head to: " + child.getId());
        }
        else if (parentEntity != null && child != null) {
            _log.debug("Deleting action in the middle of sequence");
            parentEntity.setNextAction(actionBlockEntityMapper.toActionBlockEntity(child));
            actionBlockRepository.save(parentEntity);
        }
        else{
            _log.debug("Deleting action in the end of sequence");
            parentEntity.setNextAction(null);
            actionBlockRepository.save(parentEntity);
        }
        actionBlockToDelete.setNextAction(null);
        ActionBlockEntity actionBlockEntityToDelete =
                actionBlockRepository.save(actionBlockEntityMapper.toActionBlockEntity(actionBlockToDelete));
        actionBlockRepository.delete(actionBlockEntityToDelete);
    }

    public void runAction(ActionBlock actionBlock, String fullAction) throws Exception {
        _log.info("Attempting to run acitonBlock id=" + actionBlock.getId() + ";type=" + actionBlock.getType() +";" +
                "command=" + fullAction);
        fullAction = fullAction.trim();
        ActionBlock.ActionType type = ActionBlock.ActionType.valueOf(actionBlock.getType());
        switch(type) {
            case adb: {
                System.out.println(ActionBlock.ActionType.adb);
                adbService.runCommand(fullAction);
                break;
            }
            case avdEmulator: {
                System.out.println(ActionBlock.ActionType.avdEmulator);
                avdTelnetClientService.sendCommandConnectionCheck(fullAction);
                break;
            }
            case koodous: {
                System.out.println(ActionBlock.ActionType.koodous);
                break;
            }
            case virustotal: {
                System.out.println(ActionBlock.ActionType.virustotal);
                break;
            }
            default:
                break;
        }
    }
}
