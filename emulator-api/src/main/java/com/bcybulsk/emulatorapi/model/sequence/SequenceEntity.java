package com.bcybulsk.emulatorapi.model.sequence;

import com.bcybulsk.emulatorapi.model.actionblock.ActionBlock;
import com.bcybulsk.emulatorapi.model.actionblock.ActionBlockEntity;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "sequence")
public class SequenceEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name", nullable = false, unique = true)
    private String name;

    @Column(name = "number_of_blocks", nullable = false, unique = false)
    private int numberOfBlocks;

    @Enumerated(EnumType.STRING)
    @Column(name = "status", nullable = false, unique = false)
    private Sequence.SequenceStatus status;

    @Column(name = "exception_message", nullable = true, unique = false)
    private String exceptionMessage;

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = false)
    @JoinColumn(name = "head", referencedColumnName = "id", nullable = true, unique = true)
    private ActionBlockEntity head;

    @Column(name = "owner", nullable = false, updatable = false)
    private String owner;

    @Column(name = "start_time", nullable = true, updatable = true)
    private LocalDateTime startTime;

    @Column(name = "finish_time", nullable = true, updatable = true)
    private LocalDateTime finishTime;
}
