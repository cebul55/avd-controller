package com.bcybulsk.emulatorapi.model.sequence;

import com.bcybulsk.emulatorapi.model.actionblock.*;
import com.bcybulsk.emulatorapi.model.error.TechnicalException;
import com.bcybulsk.emulatorapi.system.async.FutureMap;
import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.time.LocalDateTime;
import java.util.List;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class SequenceService {

    private final SequenceEntityMapper sequenceEntityMapper;
    private final SequenceRepository sequenceRepository;
    private final ActionBlockService actionBlockService;
    private static final Logger _log = LogManager.getLogger(SequenceService.class);
    private final FutureMap futureMap;
    private final ActionBlockEntityMapper actionBlockEntityMapper;

    @Transactional
    public List<Sequence> findAll() {
        return sequenceRepository.findAll().stream()
                .map(sequenceEntityMapper::toSequence)
                .collect(Collectors.toList());
    }

    @Transactional
    public Sequence add(Sequence sequence) {
        ActionBlock headActionBlock = sequence.getHead();
        if (headActionBlock != null && headActionBlock.getId() != null)
            sequence.setHead(actionBlockService.findById(headActionBlock.getId()));
        else if (headActionBlock != null ) {
            ActionBlock head = actionBlockService.save(headActionBlock);
            sequence.setHead(head);
        }
        sequence.setStatus(String.valueOf(Sequence.SequenceStatus.ready));
        sequence.setNumberOfBlocks(countNumberOfBlocks(sequence.getHead()));
        return sequenceEntityMapper.toSequence(sequenceRepository.save(sequenceEntityMapper.toSequenceEntity(sequence)));
    }

    private int countNumberOfBlocks(ActionBlock head) {
        if(head == null)
            return 0;
        int numberOfBlocks = 1;
        ActionBlock block = head;
        do {
            numberOfBlocks++;
            block = block.getNextAction();
        } while(block != null);
        return numberOfBlocks;
    }

    @Transactional
    public Sequence findByName(String name) {
        return sequenceRepository.findByName(name)
                .map(sequenceEntityMapper::toSequence)
                .orElse(null);
    }

    @Transactional
    public Sequence findById(Long id) {
        return sequenceRepository.findById(id)
                .map(sequenceEntityMapper::toSequence)
                .orElseThrow(
                        () -> new TechnicalException("error.sequence-not-exist",
                        new String[] {String.valueOf(id)})
                );
    }

    @Transactional
    public Sequence addNextActionBlock(Sequence sequence, ActionBlock actionBlockToAdd) {
        ActionBlock head = sequence.getHead();
        // todo numering blocks
        _log.debug("addNextActionBlock iterating over action sequence");
        if(head != null && head.getNextAction() != null) {
            // do iterating
            ActionBlock nextAction = head.getNextAction();
            while(nextAction.getNextAction() != null)
                nextAction = nextAction.getNextAction();
            _log.debug("last action is: " + nextAction.toString());
            nextAction.setNextAction(actionBlockToAdd);
            actionBlockService.save(nextAction);
        }
        else if (head != null) {
            _log.debug("addNextActionBlock add second action directly to head");

            head.setNextAction(actionBlockToAdd);
            actionBlockService.save(head);
        }
        else {
            _log.debug("addNextActionBlock add first action");
            sequence.setHead(actionBlockToAdd);
            sequence = sequenceEntityMapper.toSequence(
                    sequenceRepository.save(sequenceEntityMapper.toSequenceEntity(sequence))
            );
        }
        return sequence;
    }

    @Transactional
    public void deleteAllActions(Sequence sequence) {
        ActionBlock actionBlockToDelete = null;
        ActionBlock nextAction = null;
        if (sequence.getHead() != null){
            nextAction = sequence.getHead();
            sequence.setHead(null);
            sequence.setNumberOfBlocks(0);
            sequence.setExceptionMessage("");
            sequence.setStatus(String.valueOf(Sequence.SequenceStatus.ready));
            sequenceRepository.save(sequenceEntityMapper.toSequenceEntity(sequence));

            while(nextAction != null) {
                actionBlockToDelete = nextAction;
                nextAction = actionBlockToDelete.getNextAction();

                actionBlockToDelete.setNextAction(null);
                actionBlockToDelete = actionBlockService.save(actionBlockToDelete);
                actionBlockService.delete(actionBlockToDelete);
            }
        }
    }

    @Transactional
    public Sequence prepareToRunSequence(Sequence sequence) {
        // todo executing sequence
        // sequence with 0 action blocks is immediately finished
        ActionBlock actionBlock = sequence.getHead();
        if(actionBlock == null || sequence.getNumberOfBlocks() == 0) {
            sequence.setStatus(String.valueOf(Sequence.SequenceStatus.exception));
            sequence.setExceptionMessage("Could not start. No action blocks found.");
            sequenceRepository.save(sequenceEntityMapper.toSequenceEntity(sequence));
            throw new TechnicalException("sequence.cannot-run-null-head");
        }
        sequence.setStatus(String.valueOf(Sequence.SequenceStatus.started));
        sequence.setExceptionMessage("");
        sequence.setStartTime(LocalDateTime.now());
        sequence.setFinishTime(null);
        sequence = sequenceEntityMapper.toSequence(sequenceRepository.save(sequenceEntityMapper.toSequenceEntity(sequence)));
        return sequence;
    }

    @Async("threadActionExecutor-")
    @Transactional
    public Future runSequence(Sequence sequence) {
        _log.debug("Attempting to run sequence id=" + sequence.getId() + ";name=" + sequence.getName() +";" +
                "numberOfBlocks=" + sequence.getNumberOfBlocks());
        ActionBlock actionBlock = sequence.getHead();
        int i = 1;
        try{
            BufferedWriter writer = new BufferedWriter(new FileWriter("test.txt", true));
            do {
                _log.debug("running " + i++);
                String fullAction = actionBlock.getCommandAction()
                        + " " + actionBlock.getSubCommand1()
                        + " " + actionBlock.getSubCommand2()
                        + " " + actionBlock.getSubCommand3();
                actionBlockService.runAction(actionBlock, fullAction);
                int seconds = actionBlock.getSleepSeconds();
                TimeUnit.SECONDS.sleep(seconds);
                // get next action
                actionBlock = actionBlock.getNextAction();
            } while (actionBlock != null);
            sequence.setStatus(String.valueOf(Sequence.SequenceStatus.finished));
            sequence.setFinishTime(LocalDateTime.now());
            // removing map
            futureMap.popSequenceFuture(sequence.getId());
        }
        catch (InterruptedException interruptedException) {
            // sequence execution stopped by user
            _log.debug("Interrupted execution of sequence=" + sequence.getId());
            sequence.setStatus(String.valueOf(Sequence.SequenceStatus.force_stopped));
        }
        catch (Exception e) {
            _log.error(e.getMessage(), e.fillInStackTrace());
            sequence.setExceptionMessage(e.getMessage());
            sequence.setStatus(String.valueOf(Sequence.SequenceStatus.exception));
        }
        finally {
            sequence = sequenceEntityMapper.toSequence(sequenceRepository.save(sequenceEntityMapper.toSequenceEntity(sequence)));
        }
        // result is not important, we have to return Future in order to be able to interrupt the method execution
        return null;
    }

    @Transactional
    public void deleteSequence(Sequence sequence) {
        deleteAllActions(sequence);
        sequence = findById(sequence.getId());
        sequenceRepository.delete(sequenceEntityMapper.toSequenceEntity(sequence));
    }

    public Sequence addHead(Sequence sequence, ActionBlock actionBlockToAdd) {
        ActionBlock head = sequence.getHead();
        // todo numering blocks
        _log.debug("addNextActionBlock iterating over action sequence");
        if (head != null) {
            _log.debug("addNextActionBlock add second action directly to head");
            actionBlockToAdd.setNextAction(head);
            sequence.setHead(actionBlockToAdd);
            actionBlockService.save(actionBlockToAdd);
        }
        else {
            _log.debug("addNextActionBlock add first action");
            sequence.setHead(actionBlockToAdd);
        }
        sequence = sequenceEntityMapper.toSequence(
                sequenceRepository.save(sequenceEntityMapper.toSequenceEntity(sequence))
        );
        return sequence;
    }

    public Sequence updateActionList(Sequence sequence, String[] actionBlockIds) {
        sequence.setHead(null);
        SequenceEntity sequenceEntity = sequenceRepository.save(sequenceEntityMapper.toSequenceEntity(sequence));
        sequence = sequenceEntityMapper.toSequence(sequenceEntity);
        ActionBlock actionBlock;
        ActionBlock nextActionBlock;
        for(int i = 0; i < actionBlockIds.length; i++) {
            Long id = Long.parseLong(actionBlockIds[i]);
            actionBlock = actionBlockService.findById(id);
            actionBlock.setNextAction(null);
            actionBlockService.save(actionBlock);
        }

        for(int i = 0; i < actionBlockIds.length; i++) {
            Long id = Long.parseLong(actionBlockIds[i]);
            actionBlock = actionBlockService.findById(id);
            if(i == 0) {
                sequence.setHead(actionBlock);
                sequence =
                        sequenceEntityMapper.toSequence(sequenceRepository.save(sequenceEntityMapper.toSequenceEntity(sequence)));
            }
            if(i + 1 < actionBlockIds.length) {
                Long nextid = Long.parseLong(actionBlockIds[i+1]);
                nextActionBlock = actionBlockService.findById(nextid);
                actionBlock.setNextAction(nextActionBlock);
                actionBlockService.save(actionBlock);
            }
        }
        return sequence;
    }
}
