package com.bcybulsk.emulatorapi.model.devicestate;

import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;

@Component
public class DeviceStateMap {

    private static Map<String, DeviceState> deviceStateMap;

    @PostConstruct
    public void init() {
        deviceStateMap = new HashMap<String, DeviceState>();
    }

    public DeviceState getDeviceState(String deviceName) {
        return deviceStateMap.getOrDefault(deviceName, null);
    }

    public void putDeviceState(String deviceName, DeviceState deviceState) {
        if(deviceStateMap.getOrDefault(deviceName, null) != null)
            deviceStateMap.replace(deviceName, deviceState);
        else
            deviceStateMap.put(deviceName, deviceState);
    }

    public void popDeviceState(String deviceName) {
        deviceStateMap.remove(deviceName);
    }
}
