package com.bcybulsk.emulatorapi.model.adb;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
public class AdbPackage {
    private String packageName;
    private String fullPackageName;
    private String mainActivity;
}
