package com.bcybulsk.emulatorapi.model.sensor;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class SensorEntityMapper {

    public Sensor toSensor(SensorEntity sensorEntity) {
        return Sensor.builder()
                .id(sensorEntity.getId())
                .name(String.valueOf(sensorEntity.getName()))
                .status(String.valueOf(sensorEntity.getStatus()))
                .value(sensorEntity.getValue())
                .build();
    }

    public SensorEntity toSensorEntity(Sensor sensor) {
        return SensorEntity.builder()
                .id(sensor.getId())
                .name(Sensor.SensorName.valueOf(sensor.getName()))
                .status(Sensor.SensorState.valueOf(sensor.getStatus()))
                .value(sensor.getValue())
                .build();

    }
}
