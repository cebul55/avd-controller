package com.bcybulsk.emulatorapi.model.devicestate;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class DeviceState {

    private static final Double CPU_OVERUSE_LEVEL = 80.0;
    private static final int CPU_OVERUSE_TIME_MIN = 5;

    private String deviceId;
    /*
    ping -> expected status "I am alive!"
     */
    private boolean isAlive;
    /*
    adb shell dumpsys cpuinfo --checkin
     */
    private List<Double> cpuUsagePercentage;
    private List<LocalDateTime> statusTime;

    /* power display */
    private List<Integer> capacity;
    private List<String> status;
    private List<String> health;

    /*
    adb shell dumpsys location | grep "last location"
     Example response:
      last location=Location[gps 33.643238,-121.150333 hAcc=5 et=+18768d16h5m51s108ms etAcc=+1ms alt=0.0 vel=0.0 bear=0.0 vAcc=1 sAcc=1 bAcc=30 {Bundle[{satellites=0, maxCn0=0, meanCn0=0}]}]
      last location=null
      last location=null
      last location=Location[gps 33.643238,-121.150333 hAcc=5 et=+18768d16h5m51s108ms etAcc=+1ms alt=0.0 vel=0.0 bear=0.0 vAcc=1 sAcc=1 bAcc=30 {Bundle[{satellites=0, maxCn0=0, meanCn0=0}]}]
     */
    private List<Double> latitude;
    private List<Double> longitude;

    /*
    Sensor status
    (telnet)sensor get <sensor name>
     */
    private List<String> acceleration;
    private List<String> gyroscope;
    private List<String> magneticField;
    private List<String> orientation;
    private List<String> temperature;
    private List<String> proximity;
    private List<String> light;
    private List<String> pressure;
    private List<String> humidity;
    private List<String> magneticFieldUncalibrated;
    private List<String> gyroscopeUncalibrated;
    private List<String> hingeAngle0;
    private List<String> hingeAngle1;
    private List<String> hingeAngle2;
    private List<String> heartRate;

    /* Fall detector values */
    private Detector detector;

    public DeviceState() {
        deviceId = "";
        isAlive = true;
        cpuUsagePercentage = new ArrayList<>();
        statusTime = new ArrayList<>();
        capacity = new ArrayList<>();
        status = new ArrayList<>();
        health = new ArrayList<>();
        longitude = new ArrayList<>();
        latitude = new ArrayList<>();
        acceleration = new ArrayList<>();
        gyroscope = new ArrayList<>();
        magneticField = new ArrayList<>();
        orientation = new ArrayList<>();
        temperature = new ArrayList<>();
        proximity = new ArrayList<>();
        light = new ArrayList<>();
        pressure = new ArrayList<>();
        humidity = new ArrayList<>();
        magneticFieldUncalibrated = new ArrayList<>();
        gyroscopeUncalibrated = new ArrayList<>();
        hingeAngle0 = new ArrayList<>();
        hingeAngle1 = new ArrayList<>();
        hingeAngle2 = new ArrayList<>();
        heartRate = new ArrayList<>();
        detector = new Detector();
    }

    public void addCpuUsagePercentage(Double value) {
        cpuUsagePercentage.add(value);
        this.updateDetectorCpuStatus(value);
    }

    public void addStatusTime() {
        statusTime.add(LocalDateTime.now());
    }

    public void addBatteryStatus(String value) {
        status.add(value);
    }

    public void addBatteryCapacity(int value) {
        capacity.add(value);
    }

    public void addBatteryHealth(String value) {
        health.add(value);
    }

    public void addLocation(Double latitudeValue, Double longitudeValue) {
        latitude.add(latitudeValue);
        longitude.add(longitudeValue);
    }

    public void addAcceleration(String value) {
        acceleration.add(value);
    }

    public void addSensorValue(String sensorName, String sensorValue) {
        switch (sensorName) {
            case "acceleration": {
                acceleration.add(sensorValue);
                detector.checkAccelerometerForFall(sensorValue, statusTime.get(statusTime.size() - 1));
                detector.checkAccelerometerForLayingScreenDown(sensorValue, statusTime.get(statusTime.size() - 1));
                break;
            }
            case "gyroscope": {
                gyroscope.add(sensorValue);
                break;
            }
            case "magnetic-field": {
                magneticField.add(sensorValue);
                break;
            }
            case "orientation": {
                orientation.add(sensorValue);
                break;
            }
            case "temperature": {
                temperature.add(sensorValue);
                break;
            }
            case "proximity": {
                proximity.add(sensorValue);
                break;
            }
            case "light": {
                light.add(sensorValue);
                break;
            }
            case "pressure": {
                pressure.add(sensorValue);
                break;
            }
            case "humidity": {
                humidity.add(sensorValue);
                break;
            }
            case "magnetic-field-uncalibrated": {
                magneticFieldUncalibrated.add(sensorValue);
                break;
            }
            case "gyroscope-uncalibrated": {
                gyroscopeUncalibrated.add(sensorValue);
                break;
            }
            case "hinge-angle0": {
                hingeAngle0.add(sensorValue);
                break;
            }
            case "hinge-angle1": {
                hingeAngle1.add(sensorValue);
                break;
            }
            case "hinge-angle2": {
                hingeAngle2.add(sensorValue);
                break;
            }
            case"heart-rate": {
                heartRate.add(sensorValue);
                break;
            }
            default: {
                break;
            }
        }
    }

    public boolean updateDetectorPackageSequence(String currentPackage, LocalDateTime now) {
        return this.getDetector().updatePackageSequence(currentPackage, now);
    }

    public void updateDetectorBatteryLevel(int capacityValue) {
        this.getDetector().updateBatteryCapacity(capacityValue, LocalDateTime.now());
    }


    private void updateDetectorCpuStatus(Double value) {
        this.getDetector().updateCpuStatus(value);
    }

    public void updateDetectorBatteryStatus(String status) {
        this.getDetector().updateBatteryStatus(status);
    }
}
