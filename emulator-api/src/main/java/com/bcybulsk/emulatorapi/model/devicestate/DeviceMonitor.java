package com.bcybulsk.emulatorapi.model.devicestate;

import com.bcybulsk.emulatorapi.model.adb.AdbService;
import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.concurrent.Future;

@Service
@RequiredArgsConstructor
public class DeviceMonitor {

    private final DeviceStateMap deviceStateMap;
    private static final Logger _log = LogManager.getLogger(DeviceMonitor.class);
    private static final int SLEEP_MILISECONDS = 500;
    private final AdbService adbService;


    @Async("threadMonitor")
    public Future startMonitor(String deviceName) {
        DeviceState deviceState = deviceStateMap.getDeviceState(deviceName);
        return null;
    }
}
