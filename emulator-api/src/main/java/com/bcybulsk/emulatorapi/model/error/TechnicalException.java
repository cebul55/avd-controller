package com.bcybulsk.emulatorapi.model.error;

public class TechnicalException extends RuntimeException {
    String[] params = {};

    public TechnicalException(String errorMessage) {
        super(errorMessage);
    }

    public TechnicalException(String errorMessage, String[] params) {
        super(errorMessage);
        this.params = params;
    }

    public String[] getParams() {
        return params;
    }
}
