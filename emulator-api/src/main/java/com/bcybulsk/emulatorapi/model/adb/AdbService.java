package com.bcybulsk.emulatorapi.model.adb;

import com.android.ddmlib.*;
import com.bcybulsk.emulatorapi.model.error.TechnicalException;
import com.bcybulsk.emulatorapi.system.properties.AdbProperties;
import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.IOException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
@RequiredArgsConstructor
public class AdbService {

    private static final String PM_FULL_LISTING = "pm list packages -f"; //list all packages
    private static final String NO_ACTIVITY = "No activity found";
    private static final Pattern sPmPattern = Pattern.compile("^package:(.+?)=(.+)$"); //$NON-NLS-1$
    private static final Logger _log = LogManager.getLogger(AdbService.class);
    private final AdbProperties adbProperties;
    AndroidDebugBridge adb = null;

    private static String IS_CONNECTED = "is_Adb_Connected";
    private static String GET_DEVICES = "get_devices";
    private static String GET_INSTALLED_PACKAGES = "get_installed_package";
    private static String RUN_PACKAGE = "run_package";
    private static String GET_CURRENT_ACTIVITY = "get_current_activity";
    private static String STOP_PACKAGE = "stop_package";
    private static String INSTALL_PACKAGE = "install_package";
    private static String UNINSTALL_PACKAGE = "uninstall_package";

    @PostConstruct
    private void init() {
        AndroidDebugBridge.init(false);
        this.adb = AndroidDebugBridge.createBridge(adbProperties.getAdbLocation(), true);
    }

    @PreDestroy
    private void deinit() {
        AndroidDebugBridge.disconnectBridge();
        AndroidDebugBridge.terminate();
    }

    public boolean isAdbConnected() {
        return this.adb.isConnected();
    }

    public List<IDevice> getDeviceList() {
        IDevice[] devices = adb.getDevices();
        _log.debug("Found " + devices.length + " devices." );
        return Arrays.asList(devices.clone());
    }

    public List<AdbPackage> getInstalledPackages(String deviceId) {
        IDevice device = getDeviceBySerialNumber(deviceId);
        if(device == null)
            throw new TechnicalException("adb.device-not-found", new String[] {deviceId});

        String command = PM_FULL_LISTING;
        List<AdbPackage> installedPackages = new ArrayList<AdbPackage>();
        try {
            device.executeShellCommand(command, new MultiLineReceiver() {
                @Override
                public void processNewLines(String[] lines) {
                    if(lines.length == 1) {
                        lines = lines[0].split("\n");
                    }
                    for(String line: lines) {
                        if(!line.isEmpty()) {
                            Matcher m = sPmPattern.matcher(line);
                            if(m.matches()) {
                                String shortPackageName = line.split("=")[line.split("=").length - 1];
                                AdbPackage adbPackage = new AdbPackage(shortPackageName, line, "");

                                installedPackages.add(adbPackage);
                            }
                        }
                    }
                }

                @Override
                public boolean isCancelled() {
                    return false;
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
        List<AdbPackage> installedPackagesWithActivities = new ArrayList<AdbPackage>();
        for(AdbPackage adbPackage : installedPackages) {
            try {
                device.executeShellCommand("cmd package resolve-activity --brief " +
                        adbPackage.getPackageName() + "| tail -n 1", new MultiLineReceiver() {
                    @Override
                    public void processNewLines(String[] lines) {
                        if(lines.length > 0){
                            String mainActivity = lines[0].replaceAll("\n", "");
                            if(!mainActivity.equals(NO_ACTIVITY)) {
                                adbPackage.setMainActivity(mainActivity);
                                installedPackagesWithActivities.add(adbPackage);
                            }
                        }
                    }

                    @Override
                    public boolean isCancelled() {
                        return false;
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return installedPackagesWithActivities;
    }

    public IDevice getDeviceBySerialNumber(String deviceName) {
        IDevice[] devices = adb.getDevices();
        for(IDevice device : devices) {
            if(device.getSerialNumber().equals(deviceName))
                return device;
        }
        return null;
    }

    public IDevice getDeviceByName(String deviceName) {
        IDevice[] devices = adb.getDevices();
        for(IDevice device : devices) {
            if(device.getAvdName().equals(deviceName))
                return device;
        }
        return null;
    }

    public void runPackage(String deviceId, String packageName) throws Exception {
        IDevice device = getDeviceBySerialNumber(deviceId);
        if(device == null)
            throw new TechnicalException("adb.device-not-found", new String[] {deviceId});
        String command = "am start -n " + packageName;
        device.executeShellCommand(command, new MultiLineReceiver() {
            @Override
            public void processNewLines(String[] lines) {
                for(String line: lines) {
                    if(line.toLowerCase().contains("error") || line.toLowerCase().contains("exception"))
                        throw new TechnicalException("adb.package-not-found",
                                new String[]{Arrays.toString(lines)});
                }
                _log.info("Execution result for command=" + command + ";result=" + Arrays.toString(lines));
            }

            @Override
            public boolean isCancelled() {
                return false;
            }
        });
    }

    public String getCurrentActivity(String deviceId) throws Exception {
        IDevice device = getDeviceBySerialNumber(deviceId);
        if(device == null) {
            device = getDeviceByName(deviceId);
            if(device == null)
                throw new TechnicalException("adb.device-not-found", new String[]{deviceId});
        }String command = "dumpsys activity activities | grep mResumedActivity";
        final String[] currentActivity = {""};
        device.executeShellCommand(command, new MultiLineReceiver() {
            @Override
            public void processNewLines(String[] lines) {
                _log.info("Current activity=" + lines);
                currentActivity[0] = Arrays.toString(lines);
            }

            @Override
            public boolean isCancelled() {
                return false;
            }
        });
        return currentActivity[0];
    }

    public void stopPackage(String deviceId, String packageName) throws Exception {
        IDevice device = getDeviceBySerialNumber(deviceId);
        if(device == null)
            throw new TechnicalException("adb.device-not-found", new String[] {deviceId});
        String command = "am force-stop " + packageName;
        device.executeShellCommand(command, new MultiLineReceiver() {
            @Override
            public void processNewLines(String[] lines) {
                for(String line: lines) {
                    if(line.toLowerCase().contains("error") || line.toLowerCase().contains("exception"))
                        throw new TechnicalException("adb.package-not-found",
                                new String[]{Arrays.toString(lines)});
                }
                _log.info("Execution result for command=" + command + ";result=" + Arrays.toString(lines));
            }

            @Override
            public boolean isCancelled() {
                return false;
            }
        });
    }

    public void installPackage(String deviceId, String apkPath) throws InstallException {
        IDevice device = getDeviceBySerialNumber(deviceId);
        if(device == null)
            throw new TechnicalException("adb.device-not-found", new String[] {deviceId});
        _log.info("Installing apk:" + apkPath + "; to device: " + deviceId);
        device.installPackage(apkPath, true, "-t", "-g");
        _log.info("Successfully installed apk:" + apkPath + "; to device: " + deviceId);
    }

    public void uninstallPackage(String deviceId, String packageName) throws InstallException {
        IDevice device = getDeviceBySerialNumber(deviceId);
        if(device == null)
            throw new TechnicalException("adb.device-not-found", new String[] {deviceId});
        _log.info("Uninstalling package:" + packageName + "; from device: " + deviceId);
        device.uninstallPackage(packageName);
        _log.info("Successfully uninstalled package:" + packageName + "; from device: " + deviceId);

    }

    public void runCommand(String fullAction) throws Exception {
        // todo logging results ...
        String[] splittedAction = fullAction.split(" ");
        if(fullAction.startsWith(IS_CONNECTED))
            isAdbConnected();
        if(fullAction.startsWith(GET_DEVICES))
            getDeviceList();
        if(fullAction.startsWith(GET_INSTALLED_PACKAGES)) {
            if(splittedAction.length < 2)
                throw new TechnicalException("adb.device-not-found", new String[] {""});
            getInstalledPackages(splittedAction[1]);
        }
        if(fullAction.startsWith(RUN_PACKAGE)) {
            if(splittedAction.length < 3)
                throw new TechnicalException("adb.device-not-found", new String[] {""});
            runPackage(splittedAction[1], splittedAction[2]);
        }
        if(fullAction.startsWith(GET_CURRENT_ACTIVITY)) {
            if(splittedAction.length < 2)
                throw new TechnicalException("adb.device-not-found", new String[] {""});
            getCurrentActivity(splittedAction[1]);
        }
        if(fullAction.startsWith(STOP_PACKAGE)) {
            if(splittedAction.length < 3)
                throw new TechnicalException("adb.device-not-found", new String[] {""});
            stopPackage(splittedAction[1], splittedAction[2]);
        }
        if(fullAction.startsWith(INSTALL_PACKAGE)) {
            if(splittedAction.length < 3)
                throw new TechnicalException("adb.device-not-found", new String[] {""});
            installPackage(splittedAction[1], splittedAction[2]);
        }
        if(fullAction.startsWith(UNINSTALL_PACKAGE)) {
            if(splittedAction.length < 3)
                throw new TechnicalException("adb.device-not-found", new String[] {""});
            uninstallPackage(splittedAction[1], splittedAction[2]);
        }
    }

    public double getCpuInfo(String deviceName) throws Exception {
        IDevice device = getDeviceBySerialNumber(deviceName);
        if(device == null) {
            device = getDeviceByName(deviceName);
            if(device == null)
                throw new TechnicalException("adb.device-not-found", new String[]{deviceName});
        }
        String command = "dumpsys cpuinfo | grep TOTAL";
        final double[] cpuValue = {0.0};
        device.executeShellCommand(command, new MultiLineReceiver() {
            @Override
            public void processNewLines(String[] lines) {
                _log.info("Execution result for command=" + command + ";result=" + Arrays.toString(lines));
                if(lines.length > 0) {
                    String cpuStringValue = lines[0].split("%")[0];
                    cpuValue[0] = Double.parseDouble(cpuStringValue);
                }
            }

            @Override
            public boolean isCancelled() {
                return false;
            }
        });

        return cpuValue[0];
    }

    public Double[] getLocation(String deviceName) throws Exception {
        IDevice device = getDeviceBySerialNumber(deviceName);
        if(device == null) {
            device = getDeviceByName(deviceName);
            if(device == null)
                throw new TechnicalException("adb.device-not-found", new String[]{deviceName});
        }
        String command = "dumpsys location | grep \"last location\"";
        final Double[] location = {0.0, 0.0};
        device.executeShellCommand(command, new MultiLineReceiver() {
            @Override
            public void processNewLines(String[] lines) {
                _log.info("Execution result for command=" + command + ";result=" + Arrays.toString(lines));
                for(String line : lines) {
                    if(line.contains("gps")) {
                        /* Example line containing gps data:
                            last location=Location[gps 33.643238,-121.150333 hAcc=5 et=+18768d16h5m51s108ms etAcc=+1ms alt=0.0 vel=0.0 bear=0.0 vAcc=1 sAcc=1 bAcc=30 {Bundle[{satellites=0, maxCn0=0, meanCn0=0}]}]
                         */
                        String[] splittedLine = line.split(" ");
                        String locationValuesSplittedWithComma = splittedLine[2];
                        String latitude = locationValuesSplittedWithComma.split(",")[0];
                        String longitude = locationValuesSplittedWithComma.split(",")[1];
                        location[0] = Double.parseDouble(latitude);
                        location[1] = Double.parseDouble(longitude);
                        break;
                    }
                }
            }

            @Override
            public boolean isCancelled() {
                return false;
            }
        });
        return location;
    }
}
