package com.bcybulsk.emulatorapi.model.sequence;

import com.bcybulsk.emulatorapi.model.actionblock.ActionBlock;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import java.time.*;

@Builder
@Getter
@Setter
public class Sequence {
    Long id;
    private String name;
    private int numberOfBlocks;
    private String status;
    private String exceptionMessage;
    private ActionBlock head;
    private String owner;
    private LocalDateTime startTime;
    private LocalDateTime finishTime;


    /*
    Lifecycle plan:
    - created - only new never runned sequences
    - ready - all that have been force stopped, finished, or got error while execute
     */
    public enum SequenceStatus {
        ready, started, force_stopped, finished, exception
    }
}
