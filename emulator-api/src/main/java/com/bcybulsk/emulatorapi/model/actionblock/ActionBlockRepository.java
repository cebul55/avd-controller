package com.bcybulsk.emulatorapi.model.actionblock;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface ActionBlockRepository extends JpaRepository<ActionBlockEntity, Long> {
    List<ActionBlockEntity> findAll();
    Optional<ActionBlockEntity> findById(Long id);
    ActionBlockEntity findByNextAction(ActionBlockEntity id);
}
