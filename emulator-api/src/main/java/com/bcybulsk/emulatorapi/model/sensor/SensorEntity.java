package com.bcybulsk.emulatorapi.model.sensor;

import lombok.*;

import javax.persistence.*;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "sensor")
public class SensorEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "name", nullable = false, unique = true)
    private Sensor.SensorName name;

    @Enumerated(EnumType.STRING)
    @Column(name = "status", nullable = false, unique = false)
    private Sensor.SensorState status;

    @Column(name = "value", nullable = true)
    private String value;
}
