package com.bcybulsk.emulatorapi.model.sensor;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@Getter
@Setter
public class Sensor {
    Long id;
    private String name;
    private String status;
    private String value;

    public enum SensorName {
        acceleration("acceleration"),
        gyroscope("gyroscope"),
        magnetic_field("magnetic-field"),
        orientation("orientation"),
        temperature("temperature"),
        proximity("proximity"),
        light("light"),
        pressure("pressure"),
        humidity("humidity"),
        magnetic_field_uncalibrated("magnetic-field-uncalibrated"),
        gyroscope_uncalibrated("gyroscope-uncalibrated"),
        hinge_angle0("hinge-angle0"),
        hinge_angle1("hinge-angle1"),
        hinge_angle2("hinge-angle2"),
        heart_rate("heart-rate");

        public final String label;

        SensorName(String label) {
            this.label = label;
        }
    }

    public enum SensorState {
        enabled, disabled;
    }
}
