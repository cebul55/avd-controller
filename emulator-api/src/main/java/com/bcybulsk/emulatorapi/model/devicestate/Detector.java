package com.bcybulsk.emulatorapi.model.devicestate;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.Period;
import java.time.ZoneOffset;

@Getter
@Setter
@NoArgsConstructor
public class Detector {
    /* Falling detection */
    private boolean isFall = false;
    private LocalDateTime startFallTime;
    private LocalDateTime endFallTIme;
    private static final double EARTH_ACCELERATION = 9.81;

    /* phone laying screen down detection */
    private boolean isLaying = false;
    private LocalDateTime startLayingTime;
    private LocalDateTime endLayingTime;
    
    /* 3 previously launched packages, string values splitted with | */
    private boolean isPatternDetected;
    private String[] packageNamesList;
    private LocalDateTime[] packageChangeDates;
    private final String[] packageSequencePattern = new String[] {
           "pl.mbank/", "com.google.android.apps.messaging/", "com.android.chrome/"
    };
    private final String IGNORE_MAIN_ACTIVITY_NAME = "com.google.android.apps.nexuslauncher/";

    // battery detector
    private int initialBatteryLevel;
    private LocalDateTime startObservationTime;
    // discharge speed set as 20% per 1 hour (3600s)
    private final Double DISCHARGE_BORDER_SPEED = 0.005555555;
    private boolean fastDischarge;
    private boolean highCpu;
    private String batteryStatus;

    // cpu detector
    //todo running aps :  adb shell ps | grep u0_

    public boolean checkAccelerometerForFall(String value, LocalDateTime detectionTime) {
        String[] values = value.split(":");
        double xAxis = Double.parseDouble(values[0]);
        double yAxis = Double.parseDouble(values[1]);
        double zAxis = Double.parseDouble(values[2]);

        double totalAcceleration = Math.sqrt(Math.pow(xAxis, 2) + Math.pow(yAxis, 2) + Math.pow(zAxis, 2));
        boolean isFalling = totalAcceleration / EARTH_ACCELERATION >= 2;
        if (!isFall && !isFalling) {
            // device was not falling nor started falling - do nothing
        } else if (!isFall && isFalling) {
            // device was not falling. It started falling right now
            isFall = isFalling;
            startFallTime = detectionTime;
            endFallTIme = detectionTime;
        } else if (isFall && isFalling) {
            // device was and is still falling, end fall time update
            endFallTIme = detectionTime;
        } else if (isFall && !isFalling) {
            // device was falling and stopped right now
            isFall = isFalling;
        }
        return isFall;
    }

    public boolean checkAccelerometerForLayingScreenDown(String value, LocalDateTime detectionTime) {
        String[] values = value.split(":");
        // if phone lays screen down it should have value of earth acceleration on z-axis acceleraton
        double zAxis = Double.parseDouble(values[2]);
        boolean isCurrentlyLying = false;
        if (zAxis < -0.9 * EARTH_ACCELERATION && zAxis > -1.1 * EARTH_ACCELERATION)
            isCurrentlyLying = true;

        if (!isLaying && !isCurrentlyLying) {
            // not laying do nothing
        } else if (!isLaying && isCurrentlyLying) {
            // started laying
            isLaying = isCurrentlyLying;
            startLayingTime = detectionTime;
            endLayingTime = detectionTime;
        } else if (isLaying && isCurrentlyLying) {
            // still laying
            endLayingTime = detectionTime;
        } else if (isLaying && !isCurrentlyLying) {
            // phone picked up
            isLaying = isCurrentlyLying;
        }
        return isLaying;
    }
    
    public boolean updatePackageSequence(String packageName, LocalDateTime changeTime) {
        if(packageName.contains(IGNORE_MAIN_ACTIVITY_NAME))
            return false;
        if(this.packageNamesList == null) {
            this.packageNamesList = new String[]{};
            this.packageChangeDates = new LocalDateTime[]{};
        }
        int len = this.packageNamesList.length;
        if (len == 0) {
            this.packageNamesList = new String[] {packageName, "", ""};
            this.packageChangeDates = new LocalDateTime[] {
                    changeTime,
                    LocalDateTime.MIN,
                    LocalDateTime.MIN
            };
        }
        else if (this.packageNamesList[len - 1].contains(packageName)) {
            // first of all we need to verify if there is a need to perform update
            // if they are equal no update is needed
            this.isPatternDetected = false;
            return this.isPatternDetected;
        }
        else if (len == 1) {
            
            this.packageNamesList = new String[] {
                    this.packageNamesList[len - 1],
                    packageName,
                    ""
            };
            this.packageChangeDates = new LocalDateTime[] {
                    this.packageChangeDates[len - 1],
                    changeTime,
                    LocalDateTime.MIN
            };

        }       
        else if (len == 2 || len == 3) {
            this.packageNamesList = new String[] {
                    this.packageNamesList[len - 2],
                    this.packageNamesList[len - 1],
                    packageName
            };
            this.packageChangeDates = new LocalDateTime[] {
                    this.packageChangeDates[len - 2],
                    this.packageChangeDates[len - 1],
                    changeTime
            };
        }
        else {
            this.packageNamesList = new String[] {
                    this.packageNamesList[len - 2],
                    this.packageNamesList[len - 1],
                    packageName,
            };
            int lenD = this.packageChangeDates.length;
            this.packageChangeDates = new LocalDateTime[] {
                    this.packageChangeDates[lenD - 2],
                    this.packageChangeDates[lenD - 1],
                    changeTime,
            };
        }
        len = this.packageNamesList.length;
        // now that we have last three packages detected
        // 1. detect matching sequence
        for(int i = 0; i < len; i++) {
            if(!this.packageNamesList[i].contains(this.packageSequencePattern[i])) {
                this.isPatternDetected = false;
                return this.isPatternDetected;
            }
        }
        long beforeEventSeconds = this.packageChangeDates[0].toEpochSecond(ZoneOffset.UTC);
        long afterEventSeconds = this.packageChangeDates[2].toEpochSecond(ZoneOffset.UTC);
        long secondsBetween = afterEventSeconds - beforeEventSeconds;
        if(secondsBetween <= 30)
            this.isPatternDetected = true;
        else
            this.isPatternDetected = false;
        return this.isPatternDetected;
   }

    public void updateBatteryCapacity(int capacityValue, LocalDateTime now) {
        if(this.startObservationTime == null) {
            this.startObservationTime = now;
            this.initialBatteryLevel = capacityValue;
        }
        else {
            long beforeEventSeconds = this.startObservationTime.toEpochSecond(ZoneOffset.UTC);
            long afterEventSeconds = now.toEpochSecond(ZoneOffset.UTC);
            Double secondsBetween = Double.valueOf(afterEventSeconds - beforeEventSeconds);
            Double capacityChange = Double.valueOf(this.initialBatteryLevel - capacityValue);
            if(capacityChange/secondsBetween > DISCHARGE_BORDER_SPEED)
                this.fastDischarge = true;
            else
                this.fastDischarge = false;
        }
    }

    public void updateCpuStatus(Double value) {
        if(value > 90.0)
            this.highCpu = true;
//        else this.highCpu = false;
    }

    public void updateBatteryStatus(String status) {
        this.batteryStatus = status;
    }
}
