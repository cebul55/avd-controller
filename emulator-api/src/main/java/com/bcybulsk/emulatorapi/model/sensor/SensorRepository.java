package com.bcybulsk.emulatorapi.model.sensor;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface SensorRepository extends JpaRepository<SensorEntity, Long> {
    List<SensorEntity> findAll();
    Optional<SensorEntity> findByName(Sensor.SensorName name);

}

