package com.bcybulsk.emulatorapi.model.sequence;

import com.bcybulsk.emulatorapi.model.actionblock.ActionBlockEntityMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class SequenceEntityMapper {

    private final ActionBlockEntityMapper actionBlockEntityMapper;

    public Sequence toSequence(SequenceEntity sequenceEntity) {
        return Sequence.builder()
                .id(sequenceEntity.getId())
                .name(sequenceEntity.getName())
                .numberOfBlocks(sequenceEntity.getNumberOfBlocks())
                .status(String.valueOf(sequenceEntity.getStatus()))
                .owner(sequenceEntity.getOwner())
                .exceptionMessage(sequenceEntity.getExceptionMessage())
                .startTime(sequenceEntity.getStartTime())
                .finishTime(sequenceEntity.getFinishTime())
                .head(sequenceEntity.getHead() != null ?
                        actionBlockEntityMapper.toActionBlock(sequenceEntity.getHead()) : null)
                .build();
    }

    public SequenceEntity toSequenceEntity(Sequence sequence) {
        return SequenceEntity.builder()
                .id(sequence.getId())
                .name(sequence.getName())
                .numberOfBlocks(sequence.getNumberOfBlocks())
                .status(Sequence.SequenceStatus.valueOf(sequence.getStatus()))
                .owner(sequence.getOwner())
                .exceptionMessage(sequence.getExceptionMessage())
                .startTime(sequence.getStartTime())
                .finishTime(sequence.getFinishTime())
                .head(sequence.getHead() != null ? actionBlockEntityMapper.toActionBlockEntity(sequence.getHead()) :
                        null)
                .build();
    }
}
