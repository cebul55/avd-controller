package com.bcybulsk.emulatorapi.model.sensor;

import com.bcybulsk.emulatorapi.model.error.TechnicalException;
import com.bcybulsk.emulatorapi.presentation.sensor.SensorController;
import com.bcybulsk.emulatorapi.presentation.sensor.SensorDto;
import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class SensorService {

    private final SensorEntityMapper sensorEntityMapper;
    private final SensorRepository sensorRepository;
    private static final Logger _log = LogManager.getLogger(SensorService.class);


    @Transactional
    public List<Sensor> findAll() {
        return sensorRepository.findAll().stream()
                .map(sensorEntityMapper::toSensor)
                .collect(Collectors.toList());
    }

    @Transactional
    public void add(Sensor sensor) {
        sensorRepository.save(sensorEntityMapper.toSensorEntity(sensor));
        _log.info("Added sensor: " + sensor.getName());
    }

    @Transactional
    public void deleteAll() {
        sensorRepository.deleteAll();
        _log.info("Cleared sensor data.");
    }

    @Transactional
    public Sensor findBySensorName(Sensor.SensorName name) {
        return sensorRepository.findByName(name)
                .map(sensorEntityMapper::toSensor)
                .orElseThrow(() -> new TechnicalException("error.sensor-not-exist", new String[] {String.valueOf(name)}));
    }

    @Transactional
    public void update(SensorDto sensorDto) {
        Sensor sensor = findBySensorName(sensorDto.getName());
        sensor.setValue(sensorDto.getValue());
        sensor.setStatus(String.valueOf(sensorDto.getStatus()));
        sensorRepository.save(sensorEntityMapper.toSensorEntity(sensor));
    }
}
