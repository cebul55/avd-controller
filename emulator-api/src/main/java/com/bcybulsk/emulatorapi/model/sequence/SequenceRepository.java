package com.bcybulsk.emulatorapi.model.sequence;

import com.bcybulsk.emulatorapi.model.actionblock.ActionBlockEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Optional;

public interface SequenceRepository extends JpaRepository<SequenceEntity, Long> {
    List<SequenceEntity> findAll();

    Optional<SequenceEntity> findByName(String name);
    Optional<SequenceEntity> findById(@NotNull Long id);
    SequenceEntity findByHead(ActionBlockEntity head);
}
