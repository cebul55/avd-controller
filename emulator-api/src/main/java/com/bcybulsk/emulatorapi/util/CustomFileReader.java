package com.bcybulsk.emulatorapi.util;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class CustomFileReader {

    public static String readFullFile(String path) throws IOException {
        Path fileName = Path.of(path);
        return Files.readString(fileName);
    }

}
