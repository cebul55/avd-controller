package com.bcybulsk.emulatorapi.util;

import com.bcybulsk.emulatorapi.telnetclient.ResponseStatus;
import org.apache.commons.net.ntp.TimeStamp;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.HashMap;
import java.util.Map;

public class ResponseBodyBuilder {

    public static Map<String, String> createResponseBody(String status, String response) {
        Map<String, String> body = new HashMap<String, String>();
        body.put("status", status);
        body.put("response", response);
        TimeStamp timeStamp = new TimeStamp(System.currentTimeMillis());
        body.put("timestamp", timeStamp.toDateString());
        return body;
    }

    public static Map<String, String> createResponseBodyWithMessage(String status, String response, String message) {
        Map<String, String> body = createResponseBody(status, response);
        body.put("message", message);
        return body;
    }

    public static ResponseEntity<Map<String, String>> notConnected() {
        String messsage = "You are not connected to AVD. Connect first!";
        Map<String, String> body = ResponseBodyBuilder.createResponseBodyWithMessage( ResponseStatus.OK.name(), messsage,
                messsage);
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(body);
    }

}
