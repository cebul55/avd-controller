package com.bcybulsk.emulatorapi.telnetclient;

import com.bcybulsk.emulatorapi.model.error.TechnicalException;
import com.bcybulsk.emulatorapi.model.sensor.Sensor;
import com.bcybulsk.emulatorapi.presentation.sensor.SensorDto;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;

@Component
public class AvdTelnetClientService {
    private static final Logger _log = LogManager.getLogger(AvdTelnetClientService.class);
    private AvdTelnetClientImpl avdTelnetClient;
    private boolean isConnected;

    @Value("${avd.emulator_telnet_host}")
    private String HOSTNAME;

    @Value("${avd.emulator_telnet_port}")
    private String PORT;

    @PostConstruct
    public void init() {
        System.out.println(HOSTNAME + PORT);
        avdTelnetClient = new AvdTelnetClientImpl(HOSTNAME, Integer.parseInt(PORT) );
        isConnected = false;
    }

    public AvdTelnetClientService() { }

    public void setUpConnection() throws IOException {
        avdTelnetClient.connect();

        avdTelnetClient.waitForAvdClienResponse();
        avdTelnetClient.authenticateAvdClient();
        avdTelnetClient.waitForAvdClienResponse();
        isConnected = true;
//      while(true) {
//          System.out.println("Enter command:");
//          Scanner sc = new Scanner(System.in);
//          String command = sc.nextLine();
//          if(command.equals("QUIT")) {
//              avdTelnetClient.disconnect();
//              break;
//          }
//          avdTelnetClient.sendAvdClientCommand(command);
//          avdTelnetClient.waitForAvdClienResponse();
//            }
//            avdTelnetClient.disconnect();
    }

    public void sendCommand(String command) throws IOException {
        avdTelnetClient.sendAvdClientCommand(command);
        avdTelnetClient.waitForAvdClienResponse();
    }

    public String[] sendCommandForResponse(String command) throws IOException {
        avdTelnetClient.sendAvdClientCommand(command);
        return avdTelnetClient.waitForAvdCLientFullResponseValue();
    }

    public void tearDownConnection() throws IOException {
        avdTelnetClient.disconnect();
        isConnected = false;
    }

    public boolean isConnected() {
        return this.isConnected;
    }

    public SensorDto checkSensor(Sensor.SensorName sensorName) throws IOException {
        avdTelnetClient.sendAvdClientCommand("sensor get " + sensorName.label);
        String response = avdTelnetClient.waitForAvdClienResponseValue();
        String value = "";
        SensorDto s = new SensorDto();
        s.setName(sensorName);
        if(response.equals("") || response.startsWith(AvdTelnetClientImpl.ERR_RESPONSE_PREFIX)) {
            s.setStatus(Sensor.SensorState.disabled);
        }
        else {
            String[] splittedVal = response.split(" = ");
            System.out.println(response);
            value = splittedVal[1];
            s.setStatus(Sensor.SensorState.enabled);
            s.setValue(value);
        }
        return s;
    }

    public void sendCommandConnectionCheck(String command) throws Exception {
        if(!isConnected())
            throw new TechnicalException("avd.client-not-connected");
        sendCommand(command);
    }
}
