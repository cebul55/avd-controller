package com.bcybulsk.emulatorapi.telnetclient;

import com.bcybulsk.emulatorapi.util.CustomFileReader;
import org.apache.commons.net.telnet.TelnetClient;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class AvdTelnetClientImpl {
    private static final Logger _log = LogManager.getLogger(AvdTelnetClientImpl.class);
    private final static String OK_RESPONSE = "OK";
    public final static String ERR_RESPONSE_PREFIX = "KO:";
    private final String hostname;
    private final int port;
    private final TelnetClient avdClient;
    private String token;
    private final String tokenPath = System.getProperty("user.home") + "/.emulator_console_auth_token";

    private InputStream is;
    private OutputStream os;
    private BufferedReader console_in;
    private BufferedWriter console_out;

    public AvdTelnetClientImpl(String hostname, int port) {
        this.hostname = hostname;
        this.port = port;
        this.avdClient = new TelnetClient();
    }

    public void connect() throws IOException {
        avdClient.connect(hostname, port);
        avdClient.setConnectTimeout(200);
        avdClient.setDefaultTimeout(200);

        is = avdClient.getInputStream();
        console_in = new BufferedReader(new InputStreamReader(is));

        os = avdClient.getOutputStream();
        console_out = new BufferedWriter(new OutputStreamWriter(os));

        getAuthenticationToken();
        _log.info("Successfully connected to AVD. Hostname: " + hostname + " on port: " + port);
    }

    public void waitForAvdClienResponse() throws IOException {
        String line;
        line = console_in.readLine();
        while(line != null && !line.equals(OK_RESPONSE) && !line.startsWith("KO:")) {
            System.out.println(line);
            line = console_in.readLine();
        }
        System.out.println(line);
        _log.debug("Ok response received, waiting for command.");
    }

    public void authenticateAvdClient() throws IOException {
        String authString = "auth " + token;
        sendAvdClientCommand(authString);
        _log.info("Successfully sent authentication token to AVD.");
    }

    private String getAuthenticationToken() throws IOException {
        token = CustomFileReader.readFullFile(tokenPath);
        return token;
    }

    public void sendAvdClientCommand(String command) throws IOException {
        _log.debug("Sending command: " + command);
        console_out.write(command + "\n");
        console_out.flush();
    }

    public void disconnect() throws IOException {
        console_out.close();
        os.close();

        console_in.close();
        is.close();

        avdClient.disconnect();
        _log.info("Successfully disconnected from AVD client");
    }

    public String waitForAvdClienResponseValue() throws IOException {
        String line;
        String returnValue = "";
        line = console_in.readLine();
        while(line != null && !line.equals(OK_RESPONSE) && !line.startsWith(ERR_RESPONSE_PREFIX)) {
            System.out.println(line);
            returnValue = line;
            line = console_in.readLine();
        }
        System.out.println(line);
        _log.debug("Ok response received, waiting for command. Returning value: " + returnValue);
        return returnValue;
    }

    public String[] waitForAvdCLientFullResponseValue() throws IOException {
        List<String> lines = new ArrayList<String>();
        String line;
        line = console_in.readLine();
        while(line != null && !line.equals(OK_RESPONSE) && !line.startsWith(ERR_RESPONSE_PREFIX)) {
            lines.add(line);
            line = console_in.readLine();
        }
        if(line != null && line.startsWith(ERR_RESPONSE_PREFIX))
            throw new IOException(line);
        System.out.println(line);
        _log.debug("Ok response received, waiting for command. Returning value: " + Arrays.toString(lines.toArray(new String[0])));
        return lines.toArray(new String[0]);
    }
}
