package com.bcybulsk.emulatorapi.telnetclient;

public enum ResponseStatus {
    OK ("OK"),
    ERROR ("ERROR");

    private final String responseStatusCode;

    ResponseStatus(String responseStatusCode) {
        this.responseStatusCode = responseStatusCode;
    }


}
