package com.bcybulsk.emulatorapi.system;

import java.util.UUID;

public class UniqueIdGenerator {

    public static String get() {
        return UUID.randomUUID().toString();
    }
}
