package com.bcybulsk.emulatorapi.system.async;

import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Future;

@Component
public class FutureMap {

    private static Map<Long, Future> futureSequenceExecutionMap;
    private static Map<String, Future> futureDeviceMonitorMap;

    @PostConstruct
    public void init() {
        futureSequenceExecutionMap = new HashMap<Long, Future>();
        futureDeviceMonitorMap = new HashMap<String, Future>();
    }

    public Future getSequenceFuture(Long id) {
        return futureSequenceExecutionMap.getOrDefault(id, null);
    }

    public void putSequenceFuture(Long id, Future future) {
        futureSequenceExecutionMap.put(id,future);
    }

    public void popSequenceFuture(Long id) {
        futureSequenceExecutionMap.remove(id);
    }

    public Future getDeviceMonitorFuture(String id) {
        return futureDeviceMonitorMap.getOrDefault(id, null);
    }

    public void putDeviceMonitorFuture(String id, Future future) {
        futureDeviceMonitorMap.put(id,future);
    }

    public void popDeviceMonitorFuture(String id) {
        futureDeviceMonitorMap.remove(id);
    }
}
