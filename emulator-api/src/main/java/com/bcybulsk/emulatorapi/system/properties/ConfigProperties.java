package com.bcybulsk.emulatorapi.system.properties;

import com.sun.istack.NotNull;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties("avd")
@Data
public class ConfigProperties {
    @NotNull
    private String frontend_origin;

    @NotNull
    private String emulator_telnet_port;

    @NotNull
    private String emulator_telnet_host;

    @NotNull
    private String default_cli;

    @NotNull
    private String error_log;

    @NotNull
    private String output_log;

    @NotNull
    private String device_name;
}
