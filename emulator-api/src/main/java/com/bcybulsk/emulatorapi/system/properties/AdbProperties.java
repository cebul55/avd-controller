package com.bcybulsk.emulatorapi.system.properties;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@PropertySource(ignoreResourceNotFound = true,
                value = "classpath:adb.properties")
@Data
public class AdbProperties {

    @Value("${adb.location}")
    private String adbLocation;

}
