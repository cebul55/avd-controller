package com.bcybulsk.emulatorapi.system.properties;


import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/*
@PropertySource(ignoreResourceNotFound=true,
        value="classpath:virustotal-${spring.profiles.active}.properties")
 */
@Component
@PropertySource(ignoreResourceNotFound=true,
        value= "classpath:externalapi.properties")
@Data
public class ExternalApiProperties {
    @Value("${vt.apiKey}")
    private String vtApiKey;

    @Value("${vt.apiAddress}")
    private String vtApiAddress;

    @Value("${koodous.apiKey}")
    private String koodousApiKey;

    @Value("${koodous.apiAddress}")
    private String koodousApiAddress;
}
