package com.bcybulsk.emulatorapi.system;

public class SpringProfile {
    public static final String SWAGGER = "swagger";
    public static final String TEST = "test";
    public static final String DEV = "dev";
}
