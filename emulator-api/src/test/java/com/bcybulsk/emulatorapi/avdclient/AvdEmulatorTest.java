package com.bcybulsk.emulatorapi.avdclient;

import com.bcybulsk.emulatorapi.EmulatorApiApplication;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.*;
import java.util.Properties;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = EmulatorApiApplication.class)
@TestPropertySource(locations = "classpath:application-macbookTest.properties")
class AvdEmulatorTest {

    @Test
    public void testRunAVD() throws IOException {
        ProcessBuilder builder = new ProcessBuilder("/bin/zsh", "-c", "emulator -avd Pixel_2_API_30 -debug init,metrics -port 5555");
        builder.redirectErrorStream(true);
        Process p = builder.start();
        BufferedReader r = new BufferedReader(new InputStreamReader(p.getInputStream()));
        String line;
        while ((line = r.readLine()) != null) {
            System.out.println(line);
        }
        line = r.readLine();
        System.out.println(line);
        assertNotEquals("", line);
    }

    @Test
    public void testRunAVDRedirOutputToFile() throws IOException, InterruptedException {
        ProcessBuilder builder = new ProcessBuilder("/bin/zsh", "-c", "emulator -avd Pixel_2_API_30 -debug init,metrics -port 5555");
        builder.redirectErrorStream(true);
//        builder.redirectInput(new File("input.txt"));
        builder.redirectOutput(new File("output.txt"));
        builder.redirectError(new File("error.txt"));
        Process p = builder.start();
//        assertNotEquals("", line);
    }

    @Test
    public void checkSimpleLifecycle() throws Exception {
        AvdEmulator controller = new AvdEmulator();
        controller.init();
        controller.startAvd();
        System.out.println("Has emulator started?");
        assertTrue(controller.getAvdProcess().isAlive());
        controller.stopAvd();
//        controller.testConfig();
        assertNull(controller.getAvdProcess());
    }

/*    @Test
    public void getAvds() throws IOException {
        ProcessBuilder processBuilder = new ProcessBuilder("/bin/zsh", "-c", "emulator -list-avds");
        Process p = processBuilder.start();

        BufferedReader reader =
                new BufferedReader(new InputStreamReader(p.getInputStream()));
        StringBuilder builder = new StringBuilder();
        String line = null;
        while ( (line = reader.readLine()) != null) {
            builder.append(line);
            builder.append(System.getProperty("line.separator"));
        }
        String result = builder.toString();
        System.out.println(result);
    }*/

}