package com.bcybulsk.emulatorapi.telnetclient;

import com.bcybulsk.emulatorapi.telnetclient.AvdTelnetClientImpl;
import com.bcybulsk.emulatorapi.util.CustomFileReader;
import org.apache.commons.io.IOUtils;
import org.apache.commons.net.telnet.TelnetClient;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.Before;
import org.junit.jupiter.api.Test;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;

import static org.assertj.core.api.Assertions.assertThat;

public class AvdTelnetClientImplTest {

    private final String OK_RESPONSE = "OK";
    private final String tokenPath = System.getProperty("user.home") + "/.emulator_console_auth_token";

    @Before
    public void setUp() {
        System.out.println(System.getProperties().toString());
        String currentDirectory = System.getProperty("user.dir");
        System.out.println(currentDirectory);
    }

    @Test
    public void testConn() {
        TelnetClient telnet = new TelnetClient();
        try {
//            telnet.setDefaultPort(5555);
            telnet.connect("127.0.0.1", 5555);

            telnet.setConnectTimeout(200);
            telnet.setDefaultTimeout(200);

            System.out.println("Is connected: " + telnet.isConnected());
            System.out.println("keep alive: " + telnet.getKeepAlive());
            System.out.println("timeout: " + telnet.getConnectTimeout());

            InputStream is = telnet.getInputStream();

//            Scanner s = new Scanner(is).useDelimiter("\n");
//            String result = s.hasNext() ? s.next() : "";
//            System.out.println(result);
//            while(s.hasNext()){
//                result = s.next();
//                System.out.println(result);
//            }
            BufferedReader in = new BufferedReader(new InputStreamReader(is));
            String line = null;
            while(!(line = in.readLine()).equals(OK_RESPONSE)) {
                System.out.println(line);
            }
            System.out.println("RESPONSE FROM TELNET CLIENT");
            System.out.println(line);

            OutputStream os = telnet.getOutputStream();
            BufferedWriter out = new BufferedWriter(new OutputStreamWriter(os));
            out.write("auth " + CustomFileReader.readFullFile(tokenPath) +"\n\n");
            out.flush();
            line = in.readLine();
            System.out.println(line);

            while((line = in.readLine()).equals(OK_RESPONSE)) {
                System.out.println(line);
            }
//            in.close();

            telnet.disconnect();

//            OutputStream ou = telnet.getOutputStream();
//            ou.
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testConnection() {
        AvdTelnetClientImpl avdTelnetClient = new AvdTelnetClientImpl("127.0.0.1", 5555);
        try {
            avdTelnetClient.connect();
            avdTelnetClient.waitForAvdClienResponse();
            avdTelnetClient.authenticateAvdClient();
            avdTelnetClient.waitForAvdClienResponse();
            avdTelnetClient.disconnect();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testGivenUsingCommonsIoWithCopy()
            throws IOException {
        String originalString = "asdqweASDQWE";
        InputStream inputStream = new ByteArrayInputStream(originalString.getBytes());

        StringWriter writer = new StringWriter();
        String encoding = StandardCharsets.UTF_8.name();
        IOUtils.copy(inputStream, writer, encoding);
        System.out.println(writer.toString());
        assertEquals(writer.toString(), originalString);
    }


}
