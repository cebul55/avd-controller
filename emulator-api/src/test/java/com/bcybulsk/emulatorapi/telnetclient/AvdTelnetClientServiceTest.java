package com.bcybulsk.emulatorapi.telnetclient;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;

@RunWith(SpringRunner.class)
@SpringBootTest
@TestPropertySource(locations = "classpath:application-macbookTest.properties")
class AvdTelnetClientServiceTest {


    @Test
    void setUpConnection() throws IOException {
        AvdTelnetClientService controller = new AvdTelnetClientService();
        controller.init();
        controller.setUpConnection();
    }

    @Test
    void setUpCommandsTearDown() throws IOException {
        AvdTelnetClientService controller = new AvdTelnetClientService();
        controller.init();
        controller.setUpConnection();
        controller.sendCommand("help");
        controller.sendCommand("sms 123 hello");
        controller.sendCommand("sms send 123 hello");
        controller.tearDownConnection();
    }
}