package com.bcybulsk.emulatorapi.util;

import org.junit.After;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import static org.junit.jupiter.api.Assertions.*;

class CustomFileReaderTest {

    private final String content = "Hello Test";
    private final String filename = "src/test/resources/test.txt";

    @Before
    void setUp() throws IOException {
        BufferedWriter writer = new BufferedWriter(new FileWriter(filename));
        writer.append(content);
        writer.close();
    }

    @After
    void tearDown() {
        File testFile = new File(filename);
        testFile.delete();
    }

    @Test
    void readFullFile() throws IOException {
        String readFile = CustomFileReader.readFullFile(filename);
        assertEquals(content, readFile);
    }

    @Test
    void testBooleanStatement() {
        String value = "0:0:9.81";
        String[] values = value.split(":");
        double xAxis = Double.parseDouble(values[0]);
        double yAxis = Double.parseDouble(values[1]);
        double zAxis = Double.parseDouble(values[2]);

        double totalAcceleration = Math.sqrt(Math.pow(xAxis, 2) + Math.pow(yAxis, 2) + Math.pow(zAxis, 2));
        boolean isFalling = totalAcceleration / 9.81 >= 2;
        assertFalse(isFalling);
    }
}