package com.bcybulsk.emulatorapi.adb;

import com.android.ddmlib.AndroidDebugBridge;
import com.android.ddmlib.IDevice;
import org.aspectj.weaver.ast.And;
import org.junit.Test;

public class AdbTest {

    @Test
    public void testDoSth() {
        AndroidDebugBridge.init(false);
        AndroidDebugBridge adb = AndroidDebugBridge.createBridge("/Users/bartoszcybulski/Library/Android/sdk/platform" +
                "-tools/adb", true);

        System.out.println("Initial device lsit: " + adb.hasInitialDeviceList());
        System.out.println("Adb is connected: " + adb.isConnected());

        IDevice[] devices = adb.getDevices();
        System.out.println("FOund devices: " + devices.length);


        for(IDevice dev : devices) {
            System.out.println(dev.getAvdName());
            System.out.println(dev.getName());
            System.out.println(dev.getSerialNumber());
            System.out.println("----------------");
        }

        AndroidDebugBridge.disconnectBridge();
        AndroidDebugBridge.terminate();
    }
}
