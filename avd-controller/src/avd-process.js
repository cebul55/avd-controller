const child_process = require('child_process')
const telnet_conn = require('./telnet-connection.js')


let child_process_pid
let sub_process
const port_number = 5555

module.exports = function create_avd_process() {
    if (!child_process_pid || child_process_pid === null) {
        console.log('Starting AVD...')
        sub_process = child_process.exec('emulator -avd Pixel_2_API_30  -debug init,metrics -port ' + port_number)

        // sub_process.on('exit', function() {
        //     console.log('AVD was closed'); 
        // })

        sub_process.stdout.on('data', function (data) {
            if (data.toString().indexOf('Adb connected, start proxing data') > -1) {
                telnet_conn.connect_to_avd()
                console.log('started virtual device')
            }
            // connect_to_avd()
            // console.log('logged...')
        })
        // redirect stdout and stderr
        sub_process.stdout.pipe(process.stdout);
        sub_process.stderr.pipe(process.stderr);

        child_process_pid = sub_process.pid
        console.log('Started AVD, child process pid: ', child_process_pid)
        // connect_to_terminal()
    } else {
        console.log('Process already created')
    }
}

// function connect_to_terminal() {
//     child_process.exec('telnet localhost ' + port_number)
// }