const Telnet = require('telnet-client')
const fs = require('fs')
const homeDir = require('os').homedir();
const path = require('path')

let connection = new Telnet()

// these parameters are just examples and most probably won't work for your use-case.
var params = {
    host: '127.0.0.1',
    port: 5555,
    shellPrompt: 'OK', // or negotiationMandatory: false
    // timeout: 3000,
    // removeEcho: 4
}

// connection.connect(params)
// .then(function(prompt) {
//   connection.exec('auth q9eRN7Bu55M5D3mX')
//   .then(function(res) {
//     console.log('promises result:', res)
//     connection.exec('help-verbose').then(function(res1){
//         console.log(res1)
//         connection.exec('exit')
//     })
//   })
// }, function(error) {
//   console.log('promises reject:', error)
// })
// .catch(function(error) {
//   // handle the throw (timeout)
//   console.log(error)
// })

module.exports.connect_to_avd = function () {
    connection.connect(params)
        .then(function (prompt) {
            connection.exec('auth q9eRN7Bu55M5D3mX')
                .then(function (res) {
                    console.log('Logged in:', res)
                    connection.exec('help-verbose').then(function (res1) {
                        console.log(res1)
                    })
                })
        }, function (error) {
            console.log('promises reject:', error)
        })
        .catch(function (error) {
            // handle the throw (timeout)
            console.log(error)
        })
}

module.exports.send_sms = function () {
    if (connection === null) {
        console.log('null')
        return
    }
    console.log('sending..')
    connection.exec('sms send 500600700 hello this is test')
        .then(function (res) {
            console.log(res)
        })
        .catch(function (error) {
            console.log(error)
        })
}

function read_auth_key() {
    const auth_file = '.emulator_console_auth_token'
    const path_to_file = path.join(homeDir, auth_file)
    let auth_key = fs.readFile(path_to_file, 'utf8', function (err, data) {
        if (err) {
            return console.log(err);
        }
        console.log(data);
    })
    return auth_key
}
