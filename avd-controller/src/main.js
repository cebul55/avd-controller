const {app, ipcMain, BrowserWindow} = require('electron')
const path = require('path')
const create_avd_process = require('./avd-process.js')
const telnet_conn = require('./telnet-connection.js')

let mainWindow

function createWindow() {
    const win = new BrowserWindow({
        width: 800,
        height: 600,
        webPreferences: {
            nodeIntegration: true
        }
    })

    win.loadFile(path.join('src', 'index.html'))
    win.webContents.openDevTools()
    return win
}

function createMainWindow() {
    if (!mainWindow || mainWindow === null) {
        console.log('Creating main window...')
        mainWindow = createWindow()
        console.log('Created main window.')
    }
}

app.whenReady().then(createMainWindow)

app.on('window-all-closed', () => {
    if (process.platform !== 'darwin') {
        app.quit()
    }
})

app.on('activate', () => {
    if (BrowserWindow.getAllWindows.length === 0) {
        createMainWindow()
    }
})

ipcMain.on('start-emulator', (event) => {
    // create subprocess in Main process after receiving event
    console.log('Received event, starting AVD...')
    create_avd_process()
    event.reply('emulator-ready')
})

ipcMain.on('rotate-emulator', _ => {
    telnet_conn.send_sms()
})