const electron = require('electron')

const ipc = electron.ipcRenderer

document.getElementById('button-start-emulator').addEventListener('click', _ => {
    ipc.send('start-emulator')
})

ipc.on('emulator-ready', _ => {
    console.log('emulator-ready')
    let rotate_button = document.getElementById('button-rotate-emulator')
    rotate_button.disabled = false
    rotate_button.addEventListener('click', _ => {
        ipc.send('rotate-emulator')
    })

})