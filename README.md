# Android Emulator Controller

Desktop application designed to control android qemu emulator.

## 1. Technologies

* Electron
* Node.js
* Android emulator

## 2. Sources

* Node.js - creating new process [Node.js Doc](https://nodejs.org/api/child_process.html#child_process_child_process_fork_modulepath_args_options)
* [Running android emulator from command line](https://developer.android.com/studio/run/emulator-commandline)
* [Connecting to emulator](https://developer.android.com/studio/run/emulator-console)
* [Electron + Java Spring backend](https://medium.com/@amitbhave10/modern-desktop-application-with-electron-js-and-spring-boot-150419db0d49)
